//
//  AppDelegate.m
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)



#import "AppDelegate.h"
#import "PhoneNumberViewController.h"
#import "CurrentLocationClass.h"
#import "JVFloatingDrawerViewController.h"
#import "JVFloatingDrawerSpringAnimator.h"
#import "AppointmentRequestVC.h"

@import GoogleMaps;


@interface AppDelegate ()
{
   
}


@end


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [GMSServices provideAPIKey:@"AIzaSyC86pzXRgHi9SpTYp3fMwnKO6XPtGI4KlA"];
    

    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
   
    // Login Session
    NSString *islogin = [[NSUserDefaults standardUserDefaults] objectForKey:@"isLogin"];
    
    if ([islogin isEqualToString:@"login"])
    {
        
        [self configureDrawerViewController];
        
         UINavigationController *nVc = [[UINavigationController alloc] initWithRootViewController:self.drawerViewController];
        
        self.window.rootViewController = nVc;
        
        
    }
    else
    {
        
        RegisterViewController *rVc = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
        
        PhoneNumberViewController *pVc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
        
        
        //UINavigationController *nVc = [[UINavigationController alloc] initWithRootViewController:self.drawerViewController];
        
        UINavigationController *nVc = [[UINavigationController alloc] initWithRootViewController:pVc];
        
        self.window.rootViewController = nVc;

        
        
    }
    
   
    [self.window makeKeyAndVisible];
    
    // Register Notification
    if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) )
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    else
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
         {
             if( !error )
             {
                 [[UIApplication sharedApplication] registerForRemoteNotifications];  // required to get the app to do anything at all about push notifications
                 NSLog( @"Push registration success." );
             }
             else
             {
                 NSLog( @"Push registration FAILED" );
                 NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                 NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );  
             }  
         }];
        [[CurrentLocationClass sharedManager] initialize];

    }     return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
  
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
   
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -- Push Notification
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"deviceToken: %@", deviceToken);
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    self.deviceToken = token;
}



-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void(^)(UIBackgroundFetchResult))completionHandler
{
    // iOS 10 will handle notifications through other methods
    
    if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO( @"10.0" ) )
    {
        NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
        // set a member variable to tell the new delegate that this is background
        return;
    }
    NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );
    
    // custom code to handle notification content
    
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
    {
        NSLog( @"INACTIVE" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
    {  
        NSLog( @"BACKGROUND" );  
        completionHandler( UIBackgroundFetchResultNewData );  
    }  
    else  
    {  
        NSLog( @"FOREGROUND" );  
        completionHandler( UIBackgroundFetchResultNewData );  
    }  
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    NSLog(@"%@", notification.request.content.userInfo);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler
{
    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.userInfo);
    
    NSDictionary *dict = (NSDictionary*)response.notification.request.content.userInfo;
    
    NSDictionary *customDict = [dict valueForKey:@"custom"];
    if (customDict != nil)
    {
        AppointmentRequestVC *pVc = [[AppointmentRequestVC alloc] initWithNibName:@"AppointmentRequestVC" bundle:nil];
        
        
        pVc.notificationDict = customDict;
        
        UINavigationController *nVc = [[UINavigationController alloc] initWithRootViewController:pVc];
        
        self.window.rootViewController = nVc;
        
    }
    
}
#pragma mark  -- Receive Notificaiton Delegate --
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"didReceiveRemoteNotification userInfo=%@", userInfo);
}
#pragma mark - Drawer View Controllers

- (JVFloatingDrawerViewController *)drawerViewController
{
    if (!_drawerViewController)
    {
        _drawerViewController = [[JVFloatingDrawerViewController alloc] init];
    }
    
    return _drawerViewController;
}


#pragma mark Sides

- (UIViewController *)leftDrawerViewController
{
    if (!_leftDrawerViewController)
    {
        _leftDrawerViewController = [[LeftDrawerViewController alloc] initWithNibName:@"LeftDrawerViewController" bundle:nil];
    }
    
    return _leftDrawerViewController;
}


#pragma mark Center


- (UIViewController *)drawerCentreViewController
{
    if (!_drawerCentreViewController)
    {
        _drawerCentreViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    }
    
    return _drawerCentreViewController;
}

- (JVFloatingDrawerSpringAnimator *)drawerAnimator
{
    if (!_drawerAnimator)
    {
        _drawerAnimator = [[JVFloatingDrawerSpringAnimator alloc] init];
    }
    
    return _drawerAnimator;
}



- (void)configureDrawerViewController
{
    self.drawerViewController.leftViewController = self.leftDrawerViewController;
    
    self.drawerViewController.centerViewController = self.drawerCentreViewController;
    
    self.drawerViewController.animator = self.drawerAnimator;
    
    self.drawerViewController.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
}



#pragma mark - Global Access Helper

+ (AppDelegate *)globalDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}


- (void)toggleLeftDrawer:(id)sender animated:(BOOL)animated
{
    [self.drawerViewController toggleDrawerWithSide:JVFloatingDrawerSideLeft animated:animated completion:nil];
}


- (void)toggleRightDrawer:(id)sender animated:(BOOL)animated
{
    [self.drawerViewController toggleDrawerWithSide:JVFloatingDrawerSideRight animated:animated completion:nil];
}


@end
