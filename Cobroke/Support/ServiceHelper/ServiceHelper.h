//
//  ServiceHelper.h
//  XPRESSOR
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 10/10/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

//@"http://52.11.203.254/ws/"
#define ServiceURL  @"http://seraphicdemos.com/cobroke/rest/"
#import "UIImageView+WebCache.h"



#import <Foundation/Foundation.h>
#import "SVProgressHUD.h"

//#import "NSString+JDString.h"


@interface ServiceHelper : NSObject

@property (nonatomic, assign) BOOL isHudShow;


+ (id)sharedManager;


- (void)sendRequestWithMethodName:(NSString *)methodName setHTTPMethod:(NSString *)setHTTPMethod params:(NSDictionary *)params completion:(void (^)(NSDictionary *result))completionBlock;

- (void)sendGetRequestWithMethodName:(NSString *)methodName completion:(void (^)(NSDictionary *result))completionBlock;

- (void)showHud;

- (void)dismisHud;

- (void)showHudWithProgress:(CGFloat)value withStatus:(NSString *)status;

- (void)setHudShow:(BOOL)show;

@end
