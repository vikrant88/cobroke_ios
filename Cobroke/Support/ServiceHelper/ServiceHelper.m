//
//  ServiceHelper.m
//  XPRESSOR
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 10/10/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "ServiceHelper.h"
//#import "RKDropdownAlert.h"
#import "Reachability.h"


@implementation ServiceHelper


+ (id)sharedManager
{
    static ServiceHelper *sharedMyManager = nil;
  
    static dispatch_once_t onceToken;
  
        dispatch_once(&onceToken, ^{
      
        sharedMyManager = [[self alloc] init];
   
    });
 
    return sharedMyManager;
}


- (void)showHud
{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    
//    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:210.0/255.0 green:0.0/255.0 blue:23.0/255.0 alpha:1.0]];
    
    [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
    
    
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    
    [SVProgressHUD setCornerRadius:10.0];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    
    [SVProgressHUD show];
}


- (void)showHudWithProgress:(CGFloat)value withStatus:(NSString *)status
{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:210.0/255.0 green:0.0/255.0 blue:23.0/255.0 alpha:1.0]];
    
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    
   
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
    
    [SVProgressHUD showProgress:value status:status];

}

- (void)dismisHud
{
    [SVProgressHUD dismiss];
}


- (void)setHudShow:(BOOL)show
{
    self.isHudShow = show;
}


- (void)sendRequestWithMethodName:(NSString *)methodName setHTTPMethod:(NSString *)setHTTPMethod params:(NSDictionary *)params completion:(void (^)(NSDictionary *result))completionBlock
{
    if (![self CheckReachability])
    {
        return;
    }
    
    if (self.isHudShow == YES)
    {
        [self showHud];
    }
    

     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, methodName]]cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    
   
    NSData *jsonData;
    
    NSString* jsonString;
    
    NSData *requestData;
    
    if (params)
    {
        jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
        
        
        jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        
        
        requestData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
      
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        
        [request setHTTPBody: requestData];

    }
    
    
    [request setHTTPMethod: setHTTPMethod];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSError *error1;
         
         [self dismisHud];
       
         if (data)
         {
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
             
             NSLog(@"print... %@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);

             NSLog(@"%@", dict);
             
            completionBlock(dict);
             
         }
     }];
}

- (void)sendGetRequestWithMethodName:(NSString *)methodName completion:(void (^)(NSDictionary *result))completionBlock
{
    if (![self CheckReachability])
    {
        return;
    }
    
    if (self.isHudShow == YES)
    {
        [self showHud];
    }
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, methodName]]cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSError *error1;
         
         [self dismisHud];
         
         if (data)
         {
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
             
             NSLog(@"%@", dict);
             
             completionBlock(dict);
             
         }
     }];
}

- (BOOL)CheckReachability
{
    BOOL returnYesNo;
    
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable)
    {
        returnYesNo = YES;
        
    }
    else
    {
        returnYesNo = NO;
        
        
//        [RKDropdownAlert show];
//        
//        
//        [RKDropdownAlert title:@"No internet" message:@"Check you internet connection" backgroundColor:[UIColor blackColor ] textColor:nil time: 3];
        
    }
   
    
    return returnYesNo;
}

-(void)samplefunct:(NSString*)name completion:(void(^)(NSString* str))completionHandler
{
    completionHandler(@"str....");
}

@end
