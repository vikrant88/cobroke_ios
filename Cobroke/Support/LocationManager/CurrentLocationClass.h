//
//  CurrentLocationClass.h
//  Cobroke
//
//  Created by Vikrant on 26/04/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrentLocationClass : NSObject<CLLocationManagerDelegate>
{
    
    CLLocationManager *locationManager;
    
}
@property(assign,nonatomic)float currentLatitude;
@property(assign,nonatomic)float currentLongitude;
@property(strong,nonatomic)CLLocation *clLocation;



+(id)sharedManager;
-(void)initialize;

@end
