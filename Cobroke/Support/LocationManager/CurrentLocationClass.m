//
//  CurrentLocationClass.m
//  Cobroke
//
//  Created by Vikrant on 26/04/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "CurrentLocationClass.h"

@implementation CurrentLocationClass

@synthesize currentLongitude,currentLatitude,clLocation;

+(id)sharedManager{
    
    
    static CurrentLocationClass *sharedMyManager = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedMyManager = [[self alloc] init];
        
    });
    
    return sharedMyManager;
    
    
}

-(void)initialize
{
    
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    }


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    NSLog(@"didUpdateToLocation: %@", newLocation);
    
    CLLocation *currentLocation = newLocation;
    clLocation = newLocation;
    
    if (currentLocation != nil) {
        
        [locationManager stopUpdatingLocation];
        
        NSLog(@"longitude....%f",currentLocation.coordinate.longitude);
                currentLongitude = currentLocation.coordinate.longitude;
        
        NSLog(@"latitude....%f",currentLocation.coordinate.latitude);
                currentLatitude = currentLocation.coordinate.latitude;
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:<#(nonnull NSNotificationName)#> object:<#(nullable id)#> userInfo:<#(nullable NSDictionary *)#>]
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"getAllProjectNotification" object:currentLocation];

    }
}

@end
