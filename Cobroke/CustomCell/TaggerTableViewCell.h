//
//  TaggerTableViewCell.h
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaggerTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *nameLbl, *phoneLbl;

@property (nonatomic, retain) IBOutlet UIButton *tagBtn, *unTagBtn;

@property (nonatomic, retain) IBOutlet UIImageView *userImg;

@end
