//
//  NotificationTableViewCell.h
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NotificationTableViewCell : UITableViewCell


@property (nonatomic, retain) IBOutlet UILabel *titleLbl, *dateLbl;

@property (nonatomic, retain) IBOutlet UIImageView *userImg;


@end
