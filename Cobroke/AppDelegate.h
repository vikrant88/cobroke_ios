//
//  AppDelegate.h
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define APPDEL (AppDelegate *)[[UIApplication sharedApplication] delegate]


#import <UIKit/UIKit.h>
#import "RegisterViewController.h"
#import "LeftDrawerViewController.h"
#import "HomeViewController.h"
#import <UserNotifications/UserNotifications.h>


@class JVFloatingDrawerViewController;
@class JVFloatingDrawerSpringAnimator;


@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;


@property (nonatomic, strong) JVFloatingDrawerViewController *drawerViewController;

@property (nonatomic, strong) JVFloatingDrawerSpringAnimator *drawerAnimator;


@property (nonatomic, strong) LeftDrawerViewController *leftDrawerViewController;

@property (nonatomic, strong) HomeViewController *drawerCentreViewController;

@property (nonatomic, strong) NSString *deviceToken;

+ (AppDelegate *)globalDelegate;

- (void)toggleLeftDrawer:(id)sender animated:(BOOL)animated;

- (void)toggleRightDrawer:(id)sender animated:(BOOL)animated;

@end

