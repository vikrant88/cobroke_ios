//
//  AppointmentRequestVC.h
//  Cobroke
//
//  Created by Vikrant on 24/04/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftDrawerViewController.h"
@class JVFloatingDrawerViewController;
@class JVFloatingDrawerSpringAnimator;

@interface AppointmentRequestVC : UIViewController
{
    // uiscroll view
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UIButton *acceptButton;
    
    IBOutlet UILabel *projectNameLb;
    
    IBOutlet UILabel *phoneLb;
    
    IBOutlet UILabel *projectTimeLb;
    
    IBOutlet UILabel *projectDateLb;
    
    IBOutlet UILabel *projectAddressLb;
    
    IBOutlet UIImageView *projectImageView;
    
}

@property(assign, nonatomic) NSInteger projectID;

@property (nonatomic, strong) JVFloatingDrawerViewController *drawerViewController;

@property (nonatomic, strong) JVFloatingDrawerSpringAnimator *drawerAnimator;


@property (nonatomic, strong) LeftDrawerViewController *leftDrawerViewController;

@property (nonatomic, strong) HomeViewController *drawerCentreViewController;

@property (nonatomic, strong) NSDictionary *notificationDict;


-(IBAction)backButtonAction:(id)sender;

- (void)toggleLeftDrawer:(id)sender animated:(BOOL)animated;

- (void)toggleRightDrawer:(id)sender animated:(BOOL)animated;

// Accept Button Action
-(IBAction)acceptButtonAction:(id)sender;

@end
