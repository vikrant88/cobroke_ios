//
//  AppointmentRequestVC.m
//  Cobroke
//
//  Created by Vikrant on 24/04/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "AppointmentRequestVC.h"
#import "AgencyRequestVC.h"
#import "ServiceHelper.h"
#import "JVFloatingDrawerViewController.h"
#import "JVFloatingDrawerSpringAnimator.h"
#import "PhoneNumberViewController.h"


@interface AppointmentRequestVC ()

@end

@implementation AppointmentRequestVC
@synthesize projectID,notificationDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self getProjectDetails];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    CGSize size =  CGSizeMake(scrollView.frame.size.width, CGRectGetMaxY(acceptButton.frame)+20);
//    [scrollView setContentSize:size];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

// Accept Button Action
-(IBAction)acceptButtonAction:(id)sender
{
    AgencyRequestVC *vc = [[AgencyRequestVC alloc] initWithNibName:@"AgencyRequestVC" bundle:nil];
    vc.labelText = @"Your request has been submitted.";
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark -- Request later Action
-(IBAction)rejectNowAction:(id)sender
{
    NSDictionary *param;
    NSString *methodName;
    if ([notificationDict objectForKey:@"time"] == nil)
    {
        NSLog(@"notification for requestNow");
        
        methodName = @"responsenotification";
        
        NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        
         param = @{@"oauth":oauthToken,@"projectid": [notificationDict valueForKey:@"projectid"],@"fromid":[notificationDict valueForKey:@"fromid"],@"flag":@"2"};
    }
    else
    {
        NSLog(@"notification for request later");
        
        methodName = @"responselater";
        
        NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];

             param = @{@"oauth":oauthToken,@"projectid": [notificationDict valueForKey:@"projectid"],@"fromid":[notificationDict valueForKey:@"fromid"],@"date":[notificationDict valueForKey:@"date"],@"time":[notificationDict valueForKey:@"time"],@"comment":[notificationDict valueForKey:@"comment"],@"flag":@"2"};
    }
    
    
        [[ServiceHelper sharedManager] sendRequestWithMethodName:methodName setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         if (result)
         {
             
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 
                 
                 [self configureDrawerViewController];
                 
                 [AppDelegate globalDelegate].drawerViewController = self.drawerViewController;
                 
                 [self.navigationController pushViewController:self.drawerViewController animated:YES];
                 
                 
                 
             }
             else
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 
                 [alert show];
             }
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
    
    
    
}


#pragma mark -- Request later Action
-(IBAction)acceptNowAction:(id)sender
{
    
    NSDictionary *param;
    NSString *methodName;
    if ([notificationDict objectForKey:@"time"] == nil)
    {
        NSLog(@"notification for requestNow");
        
        methodName = @"responsenotification";
        
        NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        
        param = @{@"oauth":oauthToken,@"projectid": [notificationDict valueForKey:@"projectid"],@"fromid":[notificationDict valueForKey:@"fromid"],@"flag":@"1"};
    }
    else
    {
        NSLog(@"notification for request later");
        
        methodName = @"responselater";
        
        NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        
        param = @{@"oauth":oauthToken,@"projectid": [notificationDict valueForKey:@"projectid"],@"fromid":[notificationDict valueForKey:@"fromid"],@"date":[notificationDict valueForKey:@"date"],@"time":[notificationDict valueForKey:@"time"],@"comment":[notificationDict valueForKey:@"comment"],@"flag":@"1"};
    }
    
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"responsenotification" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         if (result)
         {
             
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 
                 
                 [self configureDrawerViewController];
                 
                 [AppDelegate globalDelegate].drawerViewController = self.drawerViewController;
                 
                 [self.navigationController pushViewController:self.drawerViewController animated:YES];
                 
                 
                 
             }
             else
             {
                 
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Title" message:[result valueForKey:@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                     
                     NSLog(@"go to home page");
                     if ([[result valueForKey:@"msg"] isEqualToString:@"Invalid Token."])
                     {
                         
                         
                         [[NSUserDefaults standardUserDefaults] setObject:@"logout" forKey:@"isLogin"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"authToken"];
                         
                         
                         PhoneNumberViewController *pVc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
                         
                         [self.navigationController pushViewController:pVc animated:YES];
                         
                         
                     }
                     
                 }]];
                 
                 [self presentViewController:alertController animated:YES completion:nil];
                 
             }
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
    
    
    
}
#pragma mark - Drawer View Controllers

- (UIViewController *)leftDrawerViewController
{
    if (!_leftDrawerViewController)
    {
        _leftDrawerViewController = [[LeftDrawerViewController alloc] initWithNibName:@"LeftDrawerViewController" bundle:nil];
    }
    
    return _leftDrawerViewController;
}


- (UIViewController *)drawerCentreViewController
{
    if (!_drawerCentreViewController)
    {
        _drawerCentreViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    }
    
    return _drawerCentreViewController;
}
- (JVFloatingDrawerSpringAnimator *)drawerAnimator
{
    if (!_drawerAnimator)
    {
        _drawerAnimator = [[JVFloatingDrawerSpringAnimator alloc] init];
    }
    
    return _drawerAnimator;
}

- (JVFloatingDrawerViewController *)drawerViewController
{
    if (!_drawerViewController)
    {
        _drawerViewController = [[JVFloatingDrawerViewController alloc] init];
    }
    
    return _drawerViewController;
}

- (void)configureDrawerViewController
{
    self.drawerViewController.leftViewController = self.leftDrawerViewController;
    
    self.drawerViewController.centerViewController = self.drawerCentreViewController;
    
    self.drawerViewController.animator = self.drawerAnimator;
    
    self.drawerViewController.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
}

#pragma mark - Global Access Helper


- (void)toggleLeftDrawer:(id)sender animated:(BOOL)animated
{
    [self.drawerViewController toggleDrawerWithSide:JVFloatingDrawerSideLeft animated:animated completion:nil];
}


- (void)toggleRightDrawer:(id)sender animated:(BOOL)animated
{
    [self.drawerViewController toggleDrawerWithSide:JVFloatingDrawerSideRight animated:animated completion:nil];
}

#pragma mark -- Get Single Project
-(void)getProjectDetails
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    
    NSDictionary *param = @{@"oauth":oauthToken,@"projectid": [notificationDict valueForKey:@"projectid"] };
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"singleproject" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         
         
         if (result)
         {
             
             
            
             
             
             NSString *time = [result valueForKey:@"starttime"];
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                     
                     [projectImageView sd_setImageWithURL:[result valueForKey:@"projectimages"] placeholderImage:[UIImage imageNamed:@"building.jpeg"]];
                     
                     
                 });
                 
                 NSLog(@"Project name  %@",[result valueForKey:@"projectname"]);

                 projectNameLb.text = [result valueForKey:@"projectname"];
                 
                 projectAddressLb.text = [result valueForKey:@"address"];
                 
                 projectTimeLb.text = [NSString stringWithFormat:@"%@ - %@",[result valueForKey:@"starttime"],[result valueForKey:@"endtime"]];
                 
             }
             else
             {
                 
                 if ([[result valueForKey:@"msg"] isEqualToString: @"Invalid Token."])
                 {
                        PhoneNumberViewController *pVc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
                     
                     [self.navigationController pushViewController:pVc animated:YES];
                 }
                 
             }
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
