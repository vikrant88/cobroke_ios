//
//  RequestAppointmentVC.m
//  Cobroke
//
//  Created by Vikrant on 24/04/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "RequestAppointmentVC.h"
#import "AppointmentRequestVC.h"
#import "AgencyRequestVC.h"
#import "ServiceHelper.h"
#import "PhoneNumberViewController.h"
@interface RequestAppointmentVC ()
{
    NSString *dateSelected;
    NSString *timeSelected;
}
@end

@implementation RequestAppointmentVC
@synthesize projectID;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // current date
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"YYYY-MM-dd";
    NSString *currentDate = [formatter stringFromDate:[NSDate date]];
    [dateButton setTitle:currentDate forState:UIControlStateNormal];
    dateSelected = currentDate;
    
  
    
    
    
    
    // current time
    formatter.dateFormat = @"hh:mm:a";
    NSString *currentTime = [formatter stringFromDate:[NSDate date]];
    [timeBtton setTitle:currentTime forState:UIControlStateNormal];
    timeSelected =  currentTime;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


#pragma mark-- Back Button Action
-(IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -- saave button Action
-(IBAction)saveButtonAction:(id)sender
{
    AgencyRequestVC *vc = [[AgencyRequestVC alloc] initWithNibName:@"AgencyRequestVC" bundle:nil];
    
    vc.labelText = @"Your request has been submitted.";
    
    [self.navigationController pushViewController:vc animated:YES];

}
#pragma mark -- date picker Action
-(IBAction)showDatePicker:(id)sender
{
    [self.view endEditing:YES];
    
    [datePickerBgView removeFromSuperview];
    
    [timePickerBgView removeFromSuperview];
    
    datePickerBgView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height+220, self.view.frame.size.width, 220)];
    
    datePickerBgView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
    
    [self.view addSubview:datePickerBgView];
    
   [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
       
       datePickerBgView.frame = CGRectMake(0, self.view.frame.size.height-220, self.view.frame.size.width,220 );
       
   } completion:^(BOOL finished) {
       
   }];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    
    datePicker.tag = 5;
    
    datePicker.frame = CGRectMake(0, 45, datePickerBgView.frame.size.width, datePickerBgView.frame.size.height);
    
    datePicker.backgroundColor = [UIColor whiteColor];
    
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    UIButton *doneButton = [[UIButton alloc]initWithFrame:CGRectMake(datePickerBgView.frame.size.width - 100, 5, 80, 35)];
    
    doneButton.tintColor = [UIColor blueColor];
    
    [doneButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];

    doneButton.backgroundColor = [UIColor clearColor];
    
    doneButton.tag = 5;
    
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    
    [doneButton addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [datePickerBgView addSubview:doneButton];
    
    [datePickerBgView addSubview: datePicker];
}
#pragma mark -- Time picker Action
-(IBAction)showTimePicker:(id)sender
{
    [self.view endEditing:YES];

    [datePickerBgView removeFromSuperview];
    
    [timePickerBgView removeFromSuperview];
    
    timePickerBgView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height+200, self.view.frame.size.width, 220)];
    
    timePickerBgView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
    
    [self.view addSubview:timePickerBgView];
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        timePickerBgView.frame = CGRectMake(0, self.view.frame.size.height-220, self.view.frame.size.width,220 );
        
    } completion:^(BOOL finished) {
        
    }];
    
    UIDatePicker *timePicker = [[UIDatePicker alloc] init];
    
    timePicker.backgroundColor = [UIColor whiteColor];
    
    timePicker.frame = CGRectMake(0, 45, timePickerBgView.frame.size.width, timePickerBgView.frame.size.height);
    
    timePicker.datePickerMode = UIDatePickerModeTime;
    
    timePicker.tag = 10;
    
    [timePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    UIButton *doneButton = [[UIButton alloc]initWithFrame:CGRectMake(timePickerBgView.frame.size.width - 100, 0, 80, 50)];
    
     [doneButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    
    doneButton.tag = 10;
    
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    
    [doneButton addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [timePickerBgView addSubview:doneButton];
    
    
    [timePickerBgView addSubview: timePicker];
}

#pragma mark -- Date picker value chang
-(void)datePickerValueChanged:(UIDatePicker*)picker
{
    
    UIDatePicker *datePicker = (UIDatePicker*)picker;
    if (datePicker.tag == 5)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setDateFormat:@"YYYY-MM-dd"];
        
        NSString *date =  [dateFormatter stringFromDate:datePicker.date];
        
        NSLog(@"date..... %@",date);
        
        dateSelected = date;
        
        [dateButton setTitle:date forState:UIControlStateNormal];
    }
    else
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        
        timeSelected =  [dateFormatter stringFromDate:datePicker.date];
        
        [dateFormatter setDateFormat:@"HH:mm:a"];
        
        [timeBtton setTitle:[dateFormatter stringFromDate:datePicker.date] forState:UIControlStateNormal];

    }
    
}

#pragma mark -- Done toolbar action
-(void)doneAction:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    if (button.tag == 5)
    {
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            datePickerBgView.frame = CGRectMake(0, self.view.frame.size.height+220, self.view.frame.size.width,220 );
            
        } completion:^(BOOL finished) {
            
        }];
        
    }
    else
    {
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            timePickerBgView.frame = CGRectMake(0, self.view.frame.size.height+220, self.view.frame.size.width,220 );
            
        } completion:^(BOOL finished) {
            
        }];
    }
    
   
}

#pragma mark -- Request later Action
-(IBAction)requestLaterAction:(id)sender
{
    
    if (dateSelected.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Select Date" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else if (timeSelected.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Select Time" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    
    else
    {
        
  
    
    
    
    NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    NSDictionary *param = @{@"oauth":oauthToken,@"projectid": [NSString stringWithFormat:@"%lu",(unsigned long)projectID],@"date":dateSelected,@"time":timeSelected ,@"comment":textView.text};
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"requestlater" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         if (result)
         {
             
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 
                
                 
             }
             else
             {
                 
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Title" message:[result valueForKey:@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                     
                     NSLog(@"go to home page");
                     if ([[result valueForKey:@"msg"] isEqualToString:@"Invalid Token."])
                     {
                         
                         
                         [[NSUserDefaults standardUserDefaults] setObject:@"logout" forKey:@"isLogin"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"authToken"];
                         
                         
                         PhoneNumberViewController *pVc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
                         
                         [self.navigationController pushViewController:pVc animated:YES];
                         
                         
                     }
                     
                 }]];
                 
                 [self presentViewController:alertController animated:YES completion:nil];
                 
             }
             
             
             
             
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
