//
//  RequestAppointmentVC.h
//  Cobroke
//
//  Created by Vikrant on 24/04/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestAppointmentVC : UIViewController
{
    
    // uiscroll view
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UIButton *saveButton;
    
    IBOutlet UIButton *dateButton;
    
    IBOutlet UIButton *timeBtton;
    
    IBOutlet UITextView *textView;
    
    // date picker and time picker
    
    UIView *datePickerBgView;
    
    UIView *timePickerBgView;

    
}

@property(assign, nonatomic) NSInteger projectID;

-(IBAction)backButtonAction:(id)sender;

-(IBAction)saveButtonAction:(id)sender;

@end
