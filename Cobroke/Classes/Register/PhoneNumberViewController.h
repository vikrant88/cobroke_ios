//
//  PhoneNumberViewController.h
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#define appDelegates ((AppDelegate *)[[UIApplication sharedApplication] delegate])


@interface PhoneNumberViewController : UIViewController
{
    IBOutlet JVFloatLabeledTextField *phoneTf;
    
    IBOutlet UILabel *countryCodeLabel;
    
    CLLocation  *clLocation;
    
    CLLocationManager *locationManager;
}

- (IBAction)loginTap;

@end
