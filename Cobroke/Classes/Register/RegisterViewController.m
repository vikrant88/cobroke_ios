//
//  RegisterViewController.m
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//


#import "RegisterViewController.h"
#import "JVFloatingDrawerViewController.h"
#import "JVFloatingDrawerSpringAnimator.h"
#import "AgencyRequestVC.h"
#import "ServiceHelper.h"
#import "IQUIView+IQKeyboardToolbar.h"

#import "RequestAppointmentVC.h"

@interface RegisterViewController ()
{
    BOOL navigationBool;
    
    NSMutableArray *universalArray;
    

    
    NSInteger *divisionIndex;
    
    NSUInteger *subdivisionIndex;
    
    // textfield type
    NSString *tftype;
    
    UIPickerView *pickerView;
    
    
    NSArray *tempArray;
    
    // to store selected Language
    
    NSString *langSelected;
    NSString *langEnglish;
    
    // to store selected Language
    NSString *langChinese;
    // to store selected Language
    NSString *langBahasa;
    // to store selected Language
    NSString *langTamil;
    
    NSString *agencyLicNumber;
    
    NSString *divisionID;
    
    NSString *subDivisionID;
    
    // To check image is select from Gallery or not
    BOOL *isImageSelected;
    
    BOOL isScroll;
    
}

@end


@implementation RegisterViewController
@synthesize responseDict;

#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // For Initial
    isImageSelected = false;
    
    isScroll = false;
    
    
    //langSelected = @"";
    
    universalArray = [[NSMutableArray alloc] init];
    
    // gesture on userProfilePic
    
    userImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
    
    [userImageView addGestureRecognizer:tapGestureRecognizer];
    
    navigationBool = true;
    
    pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    
    [pickerView setDataSource: self];
    
    [pickerView setDelegate: self];
    
    pickerView.showsSelectionIndicator = YES;
    
    agencyTf.inputView = pickerView;
    [agencyTf addDoneOnKeyboardWithTarget:self action:@selector(agencyDoneAction:)];

    
    divisionTf.inputView = pickerView;
    [divisionTf addDoneOnKeyboardWithTarget:self action:@selector(divisionDoneAction:)];


    subDivision.inputView = pickerView;
    [subDivision addDoneOnKeyboardWithTarget:self action:@selector(subDivisionDoneAction:)];

    NSLog(@"response dict... %@",responseDict);
    
    [self setUserView];
  
    [self setupView];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark -- Gesture Action
- (void)tapGestureAction:(UITapGestureRecognizer *)gestureRecognizer{
    
    
    
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Select image from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"From library",@"From camera", nil];
    
    [action showInView:self.view];
    
    
}

#pragma mark -- agencyDoneAction
-(void)agencyDoneAction:(UIBarButtonItem*)button
{
    if ([universalArray count] > 0 )
    {
        if(!isScroll && agencyTf.text.length == 0)
        {
            agencyTf.text = [[universalArray valueForKey:@"name"] firstObject];

        }
        
    }
    
    isScroll = false;
    [self.view endEditing:YES];

}

#pragma mark -- divisionDoneAction
-(void)divisionDoneAction:(UIBarButtonItem*)button
{
    if ([[[universalArray firstObject] valueForKey:@"division"] count] > 0)
    {
        if(!isScroll  && divisionTf.text.length == 0)
        {
        divisionTf.text = [[[[universalArray firstObject] valueForKey:@"division"] valueForKey:@"divisionname"] firstObject];
        }
    }
    isScroll = false;
    [self.view endEditing:YES];

}

#pragma mark -- subDivisionDoneAction
-(void)subDivisionDoneAction:(UIBarButtonItem*)button
{
    if  ([[[universalArray firstObject] valueForKey:@"subdivisions"] count] > 0)
    {
        if(!isScroll && subDivision.text.length == 0)
        {
        subDivision.text = [[[[universalArray firstObject] valueForKey:@"subdivisions"] valueForKey:@"subdivision"]firstObject];
        }
    }
    isScroll = false;

    [self.view endEditing:YES];

}
#pragma mark - ActionSheet delegates

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( buttonIndex == 0 ) {
        
        // if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *imagePickerView =[[UIImagePickerController alloc]init];
        
        imagePickerView.allowsEditing = YES;
        
        imagePickerView.delegate = self;
        
        [imagePickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
        
        if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self presentViewController:imagePickerView animated:YES completion:nil];
            }];
            
        }
        else{
            
            [self presentViewController:imagePickerView animated:NO completion:nil];
        }
        
        
    }else if( buttonIndex == 1 ) {
        
        UIImagePickerController *imagePickerView = [[UIImagePickerController alloc] init];
        
        imagePickerView.allowsEditing = YES;
        
        imagePickerView.delegate = self;
        
        imagePickerView.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self presentViewController:imagePickerView animated:YES completion:nil];
            }];
            
        }
        else
        {
            
            [self presentViewController:imagePickerView animated:NO completion:nil];
        }
        
        
    }
}
#pragma mark - PickerDelegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage * img = [info valueForKey:UIImagePickerControllerEditedImage];
    
    NSData *imgData= UIImageJPEGRepresentation(img,0.5 /*compressionQuality*/);
    
    
    UIImage *image=[UIImage imageWithData:imgData];
    
    userImageView.image = image;
    
    // set Bool for image picker
    isImageSelected = true;
 
}
#pragma mark - Void


- (void)setupView
{
    self.navigationController.navigationBar.hidden = YES;
    
    [english setTitle:@"English" forState:UIControlStateNormal];
    
    [self changeButtonColor:english isSelectes:NO];
    
    //
   
    [chineeseBtn setTitle:@"Chinese" forState:UIControlStateNormal];
    
    [self changeButtonColor:chineeseBtn isSelectes:NO];
    
    //
    
    [bahasaBtn setTitle:@"Bahasa" forState:UIControlStateNormal];
    
    [self changeButtonColor:bahasaBtn isSelectes:NO];
    
    //
    
    [tamil setTitle:@"Tamil" forState:UIControlStateNormal];
    
    [self changeButtonColor:tamil isSelectes:NO];
    
    
    // Set value to View
    firstNameTf.text = [responseDict valueForKey:@"firstname"];
    
    lastName.text = [responseDict valueForKey:@"lastname"];
    
    licenceNumberTf.text  = [responseDict valueForKey:@"agencylicense"];
    
    agencyTf.text = [responseDict valueForKey:@"agencyname"];
    
    emailTf.text = [responseDict valueForKey:@"email"];
    
    phoneNumberTf.text = [responseDict valueForKey:@"phoneno"];
    
    divisionTf.text = [responseDict valueForKey:@"division"];
    
    subDivision.text = [responseDict valueForKey:@"subdivision"];
    
    
    agencyLicNumber = [responseDict valueForKey:@"agencylicense"];
    
    divisionID = [responseDict valueForKey:@"divisionid"];
    
    subDivisionID = [responseDict valueForKey:@"subdivisionid"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [userImageView sd_setImageWithURL:[responseDict valueForKey:@"profilepic"] placeholderImage:[UIImage imageNamed:@"user_Dummy"]];
        
    });
    
    NSArray *langArray = [responseDict valueForKey:@"languages"];
    
    for (int i = 0; i < langArray.count; i++)
    {
        
        if( [english.titleLabel.text caseInsensitiveCompare:[langArray objectAtIndex:i]] == NSOrderedSame )
        {
            NSLog(@"string matched without case");
            
            [self changeButtonColor:english isSelectes:YES];
            
            langEnglish = @"english";
            
            
        }
        
        else if ( [tamil.titleLabel.text caseInsensitiveCompare:[langArray objectAtIndex:i]] == NSOrderedSame )
        {
            [self changeButtonColor:tamil isSelectes:YES];
            langTamil = @"tamil";
            
            
        }
        else if ( [bahasaBtn.titleLabel.text caseInsensitiveCompare:[langArray objectAtIndex:i]] == NSOrderedSame )
        {
            
            [self changeButtonColor:bahasaBtn isSelectes:YES];
            
            langBahasa = @"bahasa";
            
        }
        else if ( [chineeseBtn.titleLabel.text caseInsensitiveCompare:[langArray objectAtIndex:i]] == NSOrderedSame )
        {
            
            [self changeButtonColor:chineeseBtn isSelectes:YES];
            
            langChinese = @"chinese";
            
            
        }
    }
    
    
}


#pragma mark - Change language button color

- (void)changeButtonColor:(UIButton *)button isSelectes:(BOOL)isSelect
{
    if (isSelect == YES)
    {
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
       
        button.backgroundColor = [UIColor blackColor];
        
        button.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    else
    {
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
      
        button.backgroundColor = [UIColor whiteColor];
        
        button.layer.borderColor = [UIColor blackColor].CGColor;
    }
    
    button.layer.borderWidth = 1.0;
    
    button.layer.cornerRadius = english.frame.size.height/2;
}



#pragma mark - Action
#pragma mark -- lang selection

- (IBAction)languageSelection:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.tag == 1)
    {
         if (sender.selected == YES)
         {
             langEnglish = @"english";

         }
        else
        {
            langEnglish = @"";
        }

    }
    else if (sender.tag == 2)
    {
        if (sender.selected == YES)
        {
            langChinese = @"chinese";
        }
        else
        {
            langChinese = @"";
        }
    }
    else if (sender.tag == 3)
    {
        if (sender.selected == YES)
        {
             langBahasa = @"bahasa";
        }
        else
        {
            langBahasa = @"";
        }
    }
    else
    {
        if (sender.selected == YES)
        {
            langTamil = @"tamil";
        }
        else
        {
            langTamil = @"";
        }
    }
    if (sender.selected == YES)
    {
        [self changeButtonColor:sender isSelectes:YES];
    }
    else
    {
        [self changeButtonColor:sender isSelectes:NO];
    }
}

#pragma mark -- Save Method

- (IBAction)nextScreen
{
    
    if (navigationBool)
    {
        
        [self registerUserWebService];

    }
    else
    {
        
        if (langEnglish.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langEnglish;
            }
            else
            {
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langEnglish];
            }
            
        }
        if (langChinese.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langChinese;
            }
            else
            {
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langChinese];
                
            }
        }
        if (langBahasa.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langBahasa;
            }
            else
            {
                
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langBahasa];
                
            }
        }
        if (langTamil.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langTamil;
            }
            else
            {
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langTamil];
                
            }
        }
        if (langSelected.length == 0)
        {
            langSelected = @"";
        }
        if (agencyLicNumber == nil)
        {
            agencyLicNumber = @"";
        }
        if (divisionID ==  nil)
        {
            divisionID = @"";
        }
        if (subDivisionID == nil)
        {
            subDivisionID = @"";
        }
        NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        
        NSString *imageStr;
        
        if (isImageSelected)
        {
            imageStr = [self imageToBase64:userImageView.image];
            
        }
        else
        {
            imageStr = [responseDict valueForKey:@"profilepic"];
        }
        
        
        NSDictionary *param = @{@"oauth":oauthToken,@"firstname":firstNameTf.text,@"lastname":lastName.text,@"cealicense":licenceNumberTf.text,@"agency":agencyLicNumber,@"email":emailTf.text,@"division":divisionID,@"subdivision":subDivisionID,@"profilepic":imageStr,@"language":langSelected};
        
        [[ServiceHelper sharedManager] sendRequestWithMethodName:@"updateprofile" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"response ...  %@",result);
             
             if (result)
             {
                 NSString *status = [result valueForKey:@"status"];
                 if ([status isEqualToString:@"true"])
                 {
                     
                     // Login Session
                     
                [[NSUserDefaults standardUserDefaults] setObject:@"login" forKey:@"isLogin"];
                     
                     AgencyRequestVC *vc = [[AgencyRequestVC alloc] initWithNibName:@"AgencyRequestVC" bundle:nil];
                     
                     vc.labelText = @"Your request Agency change is under process. you'll be notified shortly.";
                     
                     [self.navigationController pushViewController:vc animated:YES];

                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     
                     [alert show];
                 }
                 
                 
             }
             else
             {
                 [[ServiceHelper sharedManager] setHudShow:NO];
                 NSLog(@"Login unsuccess.... ");
                 
             }
             
         }];
        
    }
    
    
   
}

#pragma mark -- Agency Change Action
- (IBAction)AgencyChange
{
    navigationBool = false;
    
     //[[ServiceHelper sharedManager] setHudShow:YES];
    [SVProgressHUD show];
    
    [self performSelector:@selector(closeloading) withObject:nil afterDelay:2.0];
    

}

#pragma mark - Drawer View Controllers

- (JVFloatingDrawerViewController *)drawerViewController
{
    if (!_drawerViewController)
    {
        _drawerViewController = [[JVFloatingDrawerViewController alloc] init];
    }
    
    return _drawerViewController;
}


#pragma mark Sides

- (UIViewController *)leftDrawerViewController
{
    if (!_leftDrawerViewController)
    {
        _leftDrawerViewController = [[LeftDrawerViewController alloc] initWithNibName:@"LeftDrawerViewController" bundle:nil];
    }
    
    return _leftDrawerViewController;
}


#pragma mark Center


- (UIViewController *)drawerCentreViewController
{
    if (!_drawerCentreViewController)
    {
        _drawerCentreViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    }
    
    return _drawerCentreViewController;
}

- (JVFloatingDrawerSpringAnimator *)drawerAnimator
{
    if (!_drawerAnimator)
    {
        _drawerAnimator = [[JVFloatingDrawerSpringAnimator alloc] init];
    }
    
    return _drawerAnimator;
}



- (void)configureDrawerViewController
{
    self.drawerViewController.leftViewController = self.leftDrawerViewController;
    
    self.drawerViewController.centerViewController = self.drawerCentreViewController;
    
    self.drawerViewController.animator = self.drawerAnimator;
    
    self.drawerViewController.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
}



#pragma mark - Textfield Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == firstNameTf)
    {
        [licenceNumberTf becomeFirstResponder];
    }
    else if (textField == licenceNumberTf)
    {
        [agencyTf becomeFirstResponder];
    }

    else if (textField == agencyTf)
    {
        [emailTf becomeFirstResponder];
    }
    
    else if (textField == emailTf)
    {
        [phoneNumberTf becomeFirstResponder];
    }
    
    else if (textField == phoneNumberTf)
    {
        [divisionTf becomeFirstResponder];
    }
    
    else if (textField == divisionTf)
    {
        [divisionTf resignFirstResponder];
    }
    
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == agencyTf)
    {
        tftype = @"agency";
        
        universalArray = [[responseDict valueForKey:@"agencies"] mutableCopy];
        
        
        return YES;
    }
    else if(textField == divisionTf)
    {
        if (agencyTf.text.length == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Select Agency " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
          
            return NO;
        }
        else
        {
             tftype = @"division";

            [universalArray removeAllObjects];
            
            universalArray = [[self getDivision:agencyTf.text] mutableCopy];
            
            [pickerView reloadAllComponents];

            
            return YES;
        }
    }
    else if (textField == subDivision)
    {
       
        tftype = @"subdivision";
        
        if (divisionTf.text.length == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Select Division" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
            return NO;
        }
        else
        {
            
            NSArray *filterArray = [self getsubDivision:divisionTf.text tofileterArray:universalArray];
            
            universalArray = [NSMutableArray new];
            
            universalArray = [filterArray mutableCopy];
            
            [pickerView reloadAllComponents];
            
            return YES;
        }
        
    }
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    
    
}


#pragma mark - PickerView delegates
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if ([tftype isEqualToString:@"agency"])
    {
        return [universalArray count];
    }
    else if ([tftype isEqualToString:@"division"])
    {
        return [[[universalArray firstObject] valueForKey:@"division"] count];
    }
    else
    {
        return [[[universalArray firstObject] valueForKey:@"subdivisions"] count];
    }
}


- (NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    NSString *agencyName;
    
    if ([tftype isEqualToString:@"agency"])
    {
        
        agencyName = [[universalArray valueForKey:@"name"] objectAtIndex:row];

    }
    else if ([tftype isEqualToString:@"division"])
    {
        agencyName = [[[[universalArray objectAtIndex:0] objectForKey:@"division"] objectAtIndex:row] objectForKey:@"divisionname"];
    }
    else
    {
        
        agencyName = [[[[universalArray firstObject] valueForKey:@"subdivisions"] valueForKey:@"subdivision"]objectAtIndex:row];
        
    }
    
    
    
    return agencyName;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    isScroll = true;
    
    if (universalArray != nil) {
        if ([tftype isEqualToString:@"agency"])
        {
            
            agencyTf.text = [[universalArray valueForKey:@"name"] objectAtIndex:row];
            
            agencyLicNumber = [[universalArray valueForKey:@"agencylicenseno"] objectAtIndex:row];
            
            divisionTf.text = @"";
            subDivision.text = @"";
            
        }
        else if ([tftype isEqualToString:@"division"])
        {
            divisionTf.text = [[[[universalArray firstObject] valueForKey:@"division"] valueForKey:@"divisionname"] objectAtIndex:row];
            
            divisionID = [[[[universalArray firstObject] valueForKey:@"division"] valueForKey:@"divisionid"] objectAtIndex:row];
            
            subDivision.text = @"";

            
        }
        else
        {
            subDivision.text = [[[[universalArray firstObject] valueForKey:@"subdivisions"] valueForKey:@"subdivision"]objectAtIndex:row];
            
            subDivisionID = [[[[universalArray firstObject] valueForKey:@"subdivisions"] valueForKey:@"subdivisionid"]objectAtIndex:row];
        }
    }
    
  
}

#pragma mark -- loading Close
-(void)closeloading
{
    [SVProgressHUD dismiss];
    
    agencyTf.userInteractionEnabled = true;
    
    divisionTf.userInteractionEnabled = true;
    
    subDivision.userInteractionEnabled = true;
    
    changeRequestButton.hidden = true;
    
    
}

#pragma mark -- Set Old New user
-(void)setUserView
{
    
    NSLog(@"already registered..  %@",[responseDict valueForKey:@"alreadyregistered"]);
    if ([[responseDict valueForKey:@"alreadyregistered"] intValue])
    {
        agencyTf.userInteractionEnabled = false;
        
        divisionTf.userInteractionEnabled = false;
        
        subDivision.userInteractionEnabled = false;
        
        licenceNumberTf.userInteractionEnabled = false;
        
        changeRequestButton.hidden = false;
        
        
    }
    else
    {
        
        agencyTf.userInteractionEnabled = true;
        
        divisionTf.userInteractionEnabled = true;
        
        subDivision.userInteractionEnabled = true;
        
        changeRequestButton.hidden = true;
    }
    
    
}

#pragma mark -- web service
-(void)registerUserWebService
{
    NSLog(@"already registered..  %@",[responseDict valueForKey:@"alreadyregistered"]);

    [[ServiceHelper sharedManager] setHudShow:YES];
    
    if (![[responseDict valueForKey:@"alreadyregistered"] intValue])
    {

    if (agencyTf.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Choose  Agency " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else if (divisionTf.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Choose Division " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    else if (subDivision.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Choose Sub Division " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
    
    else
    {
    
        NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
        NSString *imagestr;
        
        if (langEnglish.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langEnglish;
            }
            else
            {
            langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langEnglish];
            }
            
        }
        if (langChinese.length != 0) {
            
           if (langSelected == nil)
            {
                langSelected = langChinese;
            }
            else
            {
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langChinese];
 
            }
        }
        if (langBahasa.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langBahasa;
            }
            else
            {
                
            langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langBahasa];
                
            }
        }
        if (langTamil.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langTamil;
            }
            else
            {
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langTamil];

            }
        }

        if (langSelected.length == 0)
        {
            langSelected = @"";
        }
        
        if (isImageSelected)
        {
            imagestr = [self imageToBase64:userImageView.image];

        }
        else
        {
            imagestr = [responseDict valueForKey:@"profilepic"];
        }
    
    
        NSDictionary *param = @{@"oauth":oauthToken,@"firstname":firstNameTf.text,@"lastname":lastName.text,@"cealicense":licenceNumberTf.text,@"agency":agencyLicNumber,@"email":emailTf.text,@"division":divisionID,@"subdivision":subDivisionID,@"profilepic":imagestr,@"language":langSelected};
        
        NSLog(@"param send..  %@",param);
    
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"updateprofile" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         if (result)
         {
             
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 // Login Session
                 [[NSUserDefaults standardUserDefaults] setObject:@"login" forKey:@"isLogin"];


                 
                 [self configureDrawerViewController];
                 
                 [AppDelegate globalDelegate].drawerViewController = self.drawerViewController;
                 
                 [self.navigationController pushViewController:self.drawerViewController animated:YES];
             }
             else
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 
                 [alert show];
             }
             
             
             
             
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
    }
    }
    else
    {
        if (langEnglish.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langEnglish;
            }
            else
            {
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langEnglish];
            }
            
        }
        if (langChinese.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langChinese;
            }
            else
            {
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langChinese];
                
            }
        }
        if (langBahasa.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langBahasa;
            }
            else
            {
                
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langBahasa];
                
            }
        }
        if (langTamil.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langTamil;
            }
            else
            {
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langTamil];
                
            }
        }
        
        if (langSelected.length == 0)
        {
            langSelected = @"";
        }
        if (agencyLicNumber == nil)
        {
            agencyLicNumber = @"";
        }
        if (divisionID ==  nil)
        {
            divisionID = @"";
        }
        if (subDivisionID == nil)
        {
            subDivisionID = @"";
        }
        NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        
        NSString *imageStr;
        
        if (isImageSelected)
        {
            imageStr = [self imageToBase64:userImageView.image];
            
        }
        else
        {
            imageStr = [responseDict valueForKey:@"profilepic"];
        }
        
        
        NSDictionary *param = @{@"oauth":oauthToken,@"firstname":firstNameTf.text,@"lastname":lastName.text,@"cealicense":licenceNumberTf.text,@"agency":agencyLicNumber,@"email":emailTf.text,@"division":divisionID,@"subdivision":subDivisionID,@"profilepic":imageStr,@"language":langSelected};
        
        NSLog(@"param send..  %@",param);

        
        [[ServiceHelper sharedManager] sendRequestWithMethodName:@"updateprofile" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"response ...  %@",result);
             
             if (result)
             {
                 NSString *status = [result valueForKey:@"status"];
                 if ([status isEqualToString:@"true"])
                 {
                     // Login Session
                     [[NSUserDefaults standardUserDefaults] setObject:@"login" forKey:@"isLogin"];

                     
                     [self configureDrawerViewController];
                     
                     [AppDelegate globalDelegate].drawerViewController = self.drawerViewController;
                     
                     [self.navigationController pushViewController:self.drawerViewController animated:YES];
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     
                     [alert show];
                 }
                 
             }
             else
             {
                 [[ServiceHelper sharedManager] setHudShow:NO];
                 NSLog(@"Login unsuccess.... ");
                 
             }
             
         }];
    }
}
    

#pragma mark --  image to base 64 --
-(NSString*)imageToBase64:(UIImage*)image{
    
    NSData *compressedData = UIImageJPEGRepresentation(image, .5);
    
    NSString *base64Str = [compressedData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    
    //return @"dfjdfjkljfdfjkjkfjkjkljfkljdl;f";
    return base64Str;
}

-(NSArray*)getDivision:(NSString*)agencyName
{
   NSArray *localArr = [responseDict objectForKey:@"agencies"];
    
    NSPredicate *perdicate = [NSPredicate predicateWithFormat:@"name == %@",agencyName];
    
    NSArray *filterArray = [localArr filteredArrayUsingPredicate:perdicate];
    
   // [self getsubDivision:@"ANC" tofileterArray:filterArray];
    
    return filterArray;
}

-(NSArray*)getsubDivision:(NSString*)divisionName tofileterArray:(NSArray*)array
{
    
    NSSet *set1 = [NSSet setWithArray:tempArray];
    NSSet *set2 = [NSSet setWithArray:array];
    
    if (![set1 isEqualToSet:set2]) {
        
        NSLog(@"same array");
        NSArray *localArr = [[array valueForKey:@"division"] firstObject];
        
        NSPredicate *perdicate = [NSPredicate predicateWithFormat:@"divisionname == %@",divisionName];
        
        NSArray *filterArray = [localArr filteredArrayUsingPredicate:perdicate];
        
        tempArray = filterArray;
        
        return filterArray;
    }
    return array;
    
}


@end
