//
//  OTPViewController.h
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTPViewController : UIViewController
{
     IBOutlet JVFloatLabeledTextField *otpTf;
}

@property(strong, nonatomic) NSString *oauthToken;

@property(strong, nonatomic) NSString *phoneNumber;


- (IBAction)continueTap;

@end
