//
//  OTPViewController.m
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "OTPViewController.h"
#import "RegisterViewController.h"
#import "ServiceHelper.h"

@interface OTPViewController ()

@end

@implementation OTPViewController
@synthesize oauthToken,phoneNumber;
#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupView];
    
    
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}



#pragma mark - Void


- (void)setupView
{
    self.navigationController.navigationBar.hidden = YES;
}


#pragma mark - Action

- (IBAction)backTap:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)continueTap
{
    
    if (otpTf.text.length == 0)
    {
        
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Enter 4 Digits OTP" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
    }
    else
    {
        
        NSDictionary *param = @{@"oauth":oauthToken,@"otp":otpTf.text};
        
        [[ServiceHelper sharedManager] sendRequestWithMethodName:@"verifyotp" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"response ...  %@",result);
             
             if (result)
             {
                 
                 
                 NSString *status = [result valueForKey:@"status"];
                 if ([status isEqualToString:@"true"])
                 {
                     RegisterViewController *vc = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
                     
                     vc.responseDict = result;
                     
                      [[NSUserDefaults standardUserDefaults] setObject:oauthToken forKey:@"authToken"];
                     
                     
                     [self.navigationController pushViewController:vc animated:YES];
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     
                     [alert show];
                 }
             }
             else
             {
                 [[ServiceHelper sharedManager] setHudShow:NO];
                 NSLog(@"Login unsuccess.... ");
                 
             }
             
         }];
    }
}
#pragma mark - Action Login Action
-(IBAction)resentOTP:(id)sender{
    
    NSDictionary *param = @{@"phoneno":phoneNumber};
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"resendotp" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         
         
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OTP" message:[result valueForKey:@"otp"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         
         [alert show];
         
         if (!result)
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
             

           
         }
         
     }];
   
}
@end
