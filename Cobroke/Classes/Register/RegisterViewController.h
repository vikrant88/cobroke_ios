//
//  RegisterViewController.h
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JVFloatLabeledTextField.h"


#import "LeftDrawerViewController.h"
#import "HomeViewController.h"

@class JVFloatingDrawerViewController;
@class JVFloatingDrawerSpringAnimator;


@interface RegisterViewController : UIViewController <UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource,UINavigationControllerDelegate,
UIImagePickerControllerDelegate, UIActionSheetDelegate>
{
    IBOutlet UIButton *english, *chineeseBtn, *bahasaBtn, *tamil;
    
    IBOutlet UIImageView *userImageView;
    
    IBOutlet UIButton *changeRequestButton;
    
    IBOutlet UITextField *firstNameTf,*lastName, *licenceNumberTf, *agencyTf, *emailTf, *phoneNumberTf, *divisionTf, *subDivision;
    
    
}

@property (nonatomic, strong) JVFloatingDrawerViewController *drawerViewController;

@property (nonatomic, strong) JVFloatingDrawerSpringAnimator *drawerAnimator;


@property (nonatomic, strong) LeftDrawerViewController *leftDrawerViewController;

@property (nonatomic, strong) HomeViewController *drawerCentreViewController;


@property (nonatomic, strong) NSDictionary *responseDict;

- (IBAction)languageSelection:(UIButton *)sender;

- (IBAction)nextScreen;



@end
