//
//  PhoneNumberViewController.m
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "PhoneNumberViewController.h"
#import "OTPViewController.h"
#import "ServiceHelper.h"
#import "CurrentLocationClass.h"

@interface PhoneNumberViewController ()


@end


@implementation PhoneNumberViewController


#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    [self setupView];
    
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}



#pragma mark - Void


- (void)setupView
{
   self.navigationController.navigationBar.hidden = YES;
}


#pragma mark - Action Login Action
- (IBAction)loginTap
{
    
    
    if (phoneTf.text.length < 8)
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Enter 8 Digits Mobile Number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
         
    }
    else
    {
        //NSString *deviceToken =  [[AppDelegate globalDelegate] deviceToken];
        
         NSString *deviceToken =  @"dlfjkdlfjkd54df54d5f";
        
        CLLocation *currentLocation = [[CurrentLocationClass sharedManager] clLocation];
        
         NSDictionary *param = @{@"phoneno":[NSString stringWithFormat:@"+65%@",phoneTf.text],@"devicetoken":deviceToken,@"devicetype":@"ios",@"latitude":[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude],@"longitude":[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude]};
        
//     NSDictionary *param = @{@"phoneno":[NSString stringWithFormat:@"+65%@",phoneTf.text],@"devicetoken":deviceToken,@"devicetype":@"ios",@"latitude":@"76.123",@"longitude":@"30.123"};
    
    [[ServiceHelper sharedManager] setHudShow:YES];

    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"login" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         if (result)
         {
             
              OTPViewController *vc = [[OTPViewController alloc]
                                       initWithNibName:@"OTPViewController" bundle:nil];
             
             vc.oauthToken = [result valueForKey:@"oauth"];
             
             vc.phoneNumber = [NSString stringWithFormat:@"%@%@",countryCodeLabel.text,phoneTf.text];
             
             
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"OTP" message:[result valueForKey:@"otp"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [alert show];
             
             
             [self.navigationController pushViewController:vc animated:YES];
             
         }
         else
         {
             
             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Title" message:[result valueForKey:@"msg"] preferredStyle:UIAlertControllerStyleAlert];
             
             [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                 
                 NSLog(@"go to home page");
                 if ([[result valueForKey:@"msg"] isEqualToString:@"Invalid Token."])
                 {
                     
                     
                     [[NSUserDefaults standardUserDefaults] setObject:@"logout" forKey:@"isLogin"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"authToken"];
                     
                     
                     PhoneNumberViewController *pVc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
                     
                     [self.navigationController pushViewController:pVc animated:YES];
                     
                     
                 }
                 
             }]];
             
             [self presentViewController:alertController animated:YES completion:nil];
             
         }
         
     }];
    }
    

}


@end
