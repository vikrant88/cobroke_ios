//
//  NotificationViewController.m
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationTableViewCell.h"
#import "AppointmentRequestVC.h"
#import "ServiceHelper.h"

@interface NotificationViewController ()
{
    NSMutableArray *notificationArr;
}

@end



@implementation NotificationViewController


#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupView];
    
    [self getAllNotifications];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}



#pragma mark - Void


- (void)setupView
{
      notificationArr = [[NSMutableArray alloc] initWithObjects:
                        @{@"title" : @"Jaskirat", @"date" : @"06 Apr 2017, 10:30PM"},
                        @{@"title" : @"Jaskirat", @"date" : @"06 Apr 2017, 10:30PM"},
                        @{@"title" : @"Jaskirat", @"date" : @"06 Apr 2017, 10:30PM"},
                        @{@"title" : @"Jaskirat", @"date" : @"06 Apr 2017, 10:30PM"},
                        @{@"title" : @"Jaskirat", @"date" : @"06 Apr 2017, 10:30PM"},
                        @{@"title" : @"Jaskirat", @"date" : @"06 Apr 2017, 10:30PM"},
                        @{@"title" : @"Jaskirat", @"date" : @"06 Apr 2017, 10:30PM"},
                        @{@"title" : @"Jaskirat", @"date" : @"06 Apr 2017, 10:30PM"},
                        @{@"title" : @"Jaskirat", @"date" : @"06 Apr 2017, 10:30PM"},
                        @{@"title" : @"Jaskirat", @"date" : @"06 Apr 2017, 10:30PM"},nil];
}


#pragma mark - Action


- (IBAction)openDrawer
{
    [[AppDelegate globalDelegate] toggleLeftDrawer:self animated:YES];
}



#pragma mark - Table View Delegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return notificationArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"CELLID";
    
    NotificationTableViewCell *cell = (NotificationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.titleLbl.text = [[notificationArr objectAtIndex:indexPath.row] objectForKey:@"projectname"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [cell.userImg sd_setImageWithURL:[[notificationArr objectAtIndex:indexPath.row] valueForKey:@"projectimage"]  placeholderImage:[UIImage imageNamed:@"user_Dummy"]];
        
    });

    //cell.userImg.image = [UIImage imageNamed:@"user_Dummy"];
    
    cell.dateLbl.text = [[notificationArr objectAtIndex:indexPath.row] objectForKey:@"datetime"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AppointmentRequestVC *vc = [[AppointmentRequestVC alloc] initWithNibName:@"AppointmentRequestVC" bundle:nil];
    
    vc.notificationDict = [notificationArr objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark -- Get Notification method
-(void)getAllNotifications
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    
    NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    
    NSDictionary *param = @{@"oauth":oauthToken};
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"notificationlist" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         
         
         
         if (result)
         {
             
             
             notificationArr = [result valueForKey:@"notification"];
             [notificationTbl reloadData];
             
             
             
             
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
}

@end
