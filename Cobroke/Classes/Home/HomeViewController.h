//
//  HomeViewController.h
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import <GoogleMaps/GoogleMaps.h>


@interface HomeViewController : UIViewController<CLLocationManagerDelegate>
{
    IBOutlet GMSMapView *googleMapView;
    
    IBOutlet UISearchBar *searchBarTF;
    
    CLLocationManager *locationManager;
    
    // All Project Array
    NSArray *projectArray;
    
    BOOL viewAppearBool;

}

- (IBAction)openDrawer;

@end
