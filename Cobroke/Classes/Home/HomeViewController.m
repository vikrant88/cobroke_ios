//
//  HomeViewController.m
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "HomeViewController.h"
#import "ProjectsViewController.h"
#import "ServiceHelper.h"
#import "CurrentLocationClass.h"
#import "PhoneNumberViewController.h"

@interface HomeViewController () <GMSMapViewDelegate>




@end

@implementation HomeViewController

#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    viewAppearBool = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAllProjectss:) name:@"getAllProjectNotification" object:nil];
    
    
  
    
    
    
    
    
   
}
//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [searchBarTF resignFirstResponder];
    
    [self.view endEditing:YES];

    
    //Do stuff here...
}
-(void)getAllProjectss:(NSNotification*)notification
{
    CLLocation *location = (CLLocation*)notification.object;
    
    NSLog(@"Get Location ,, %@",location);
    
    [self getCurrentLocation:location];
    
    [self getAllProjects:location];
}


- (void)viewWillAppear:(BOOL)animated
{
   
    
    
    
   
    [self setupView];
    if (viewAppearBool)
    {
        [[ServiceHelper sharedManager] showHud];

        
        if ([[CurrentLocationClass sharedManager] clLocation] != nil  )
        {
            [self getCurrentLocation:[[CurrentLocationClass sharedManager] clLocation]];
            [self getAllProjects:[[CurrentLocationClass sharedManager] clLocation]];
        }
        else
        {
            [[CurrentLocationClass sharedManager] initialize];
            
        }
        
    }
    
    viewAppearBool = NO;

    //[self performSelector:@selector(freezeApp) withObject:nil afterDelay:1.0];
    
    [super viewWillAppear:animated];
   
    
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UIGestureRecognizer *tapGesture = [[UIGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)];
    
    [googleMapView addGestureRecognizer:tapGesture];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}



#pragma mark - Void


- (void)setupView
{
    self.navigationController.navigationBar.hidden = YES;
    
    //GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            //longitude:151.20
                                                              //   zoom:6];
    //mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    
    googleMapView.myLocationEnabled = YES;
    
    googleMapView.delegate = self;
    
    
    //self.view = mapView;

//    GMSMarker *marker = [[GMSMarker alloc] init];

//    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
//    
//    marker.title = @"Sydney";
//    
//    marker.icon = [UIImage imageNamed:@"locationIcon"];
//    
//    marker.snippet = @"Australia";
//    
//    marker.map = mapView;
    

}

#pragma mark -- Add Markers
-(void)addMarkers:(NSArray*)proLocArray
{
    
    
    for(int i =0 ; i < proLocArray.count ; i++)
    {
        
        // Creates a marker in the center of the map.
        GMSMarker *marker = [[GMSMarker alloc] init];

        float lat = [[[proLocArray valueForKey:@"latitude"] objectAtIndex:i ] floatValue];
        
        float longitude  = [[[proLocArray valueForKey:@"longitude"] objectAtIndex:i ] floatValue];
        
        marker.position = CLLocationCoordinate2DMake(lat,longitude );
        
        UIImage *markerImage = [UIImage imageNamed:@"locationIcon"];

        // Main view
        
        UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, markerImage.size.width, markerImage.size.height)];
        
        
        
        // Image view
        
        UIImageView *markerImageView = [[UIImageView alloc] initWithFrame:containerView.frame];
        
        markerImageView.image = markerImage;
        
        
        //Text label
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(containerView.frame.origin.x+3, 0, containerView.frame.size.width-50, 25)];
        
        textLabel.tag = [[[proLocArray valueForKey:@"id"] objectAtIndex:i ] integerValue];
        
        textLabel.textColor = [UIColor whiteColor];
        
        textLabel.textAlignment = NSTextAlignmentLeft;
        
        textLabel.font = [UIFont fontWithName:@"CircularStd-Book" size:8];
        
        textLabel.numberOfLines = 2;
        
        //textLabel.backgroundColor = [UIColor greenColor];
        
        textLabel.text = [[proLocArray valueForKey:@"projectname"] objectAtIndex:i];
        
        [containerView addSubview:markerImageView];
        
        [containerView addSubview:textLabel];
        

        
        
        marker.zIndex = i;
        
        marker.iconView = containerView;
        
        //marker.snippet = @"Australia";
        
        marker.map = googleMapView;

        marker.map = googleMapView;
        
        
        
    }
    
    
    
}

#pragma mark -- get Current location
-(void)getCurrentLocation:(CLLocation*)location{
    
    
    NSLog(@"current lat  %f",[[CurrentLocationClass sharedManager] currentLatitude]);
    NSLog(@"current lat  %f",[[CurrentLocationClass sharedManager] currentLongitude]);

    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                            longitude:location.coordinate.longitude
                                                                 zoom:12];

     [googleMapView animateToCameraPosition:camera];
    
}


#pragma mark - Action

- (IBAction)openDrawer
{
    [[AppDelegate globalDelegate] toggleLeftDrawer:self animated:YES];
    
    
}


#pragma mark - Google Map


- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    
    NSLog(@"icon tag  %d",marker.zIndex);

    
    NSString *role = [[projectArray valueForKey:@"role"] objectAtIndex:marker.zIndex];
    
   
    
    UIView *vieww = marker.iconView;
    for (UIView *view in vieww.subviews)
    {
        if ([view isKindOfClass:[UILabel class]]) {
            
            UILabel *lb = (UILabel*)view;
            

            
            NSLog(@"label text...   %ld",lb.tag);
            
            NSLog(@"label text...   %@",lb.text);
            
            ProjectsViewController *vc = [[ProjectsViewController alloc] initWithNibName:@"ProjectsViewController" bundle:nil];
            
            
            //NSPredicate *perdicate = [NSPredicate predicateWithFormat:@""];
            
            vc.projectID = lb.tag;
            vc.projectRole = role;
            
            [self.navigationController pushViewController:vc animated:YES];
       
            
        }
        
        
    }
  
    return YES;
}


- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    
}

- (void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay
{
    
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
    
}
#pragma mark -- searchbar delegate method

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSLog(@"text...  %@",searchText);
    [self submitButtonAction:searchText];
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    NSLog(@"search Pressed");
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    NSLog(@"cancelled button clicked");
    
    [searchBarTF resignFirstResponder];
    
    [self.view endEditing:YES];
}
#pragma mark -- Submit Button Action --
-(void)submitButtonAction:(NSString*)searchText{
    
    if (!(searchText.length == 0))
    {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(projectname CONTAINS[c]  %@)",searchText];
        
        NSArray *array = [projectArray filteredArrayUsingPredicate:predicate];
        
        NSLog(@"FilteredArray  %@",array);
        
        [googleMapView clear];
        
        [self addMarkers:array];
        
    }
    else
    {
        
        [googleMapView clear];
        
        [self addMarkers:projectArray];
        
        [searchBarTF resignFirstResponder];
        
        [self.view endEditing:YES];
        
        [searchBarTF endEditing:true];

        
    }
}

#pragma mark -- Get All Projects Method
     -(void)getAllProjects:(CLLocation*)location
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    
    NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    NSString *latstr = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    
    NSString *longstr = [NSString stringWithFormat:@"%f",location.coordinate.longitude];

    NSDictionary *param = @{@"oauth":oauthToken,@"latitude":latstr,@"longitude":longstr};
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"allprojects" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         if (result)
         {
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                
                 
                 projectArray = [result valueForKey:@"allprojects"];
                 
                 NSArray *tempArray = projectArray;
                 
                 [self addMarkers:tempArray];

                 
                 
             }
             else
             {
                 
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Title" message:[result valueForKey:@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                     
                     NSLog(@"go to home page");
                     if ([[result valueForKey:@"msg"] isEqualToString:@"Invalid Token."])
                     {
                         
                         
                         [[NSUserDefaults standardUserDefaults] setObject:@"logout" forKey:@"isLogin"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"authToken"];
                         
                         
                         PhoneNumberViewController *pVc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
                         
                         [self.navigationController pushViewController:pVc animated:YES];
                         
                         
                     }
                     
                 }]];
                 
                 [self presentViewController:alertController animated:YES completion:nil];
                 
             }
             
             
             
             
             
         }
         else
         {
             [SVProgressHUD dismiss];
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
}
@end
