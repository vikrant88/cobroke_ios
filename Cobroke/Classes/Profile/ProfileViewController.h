//
//  ProfileViewController.h
//  Cobroke
//
//  Created by Vikrant on 25/04/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController<UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource,UINavigationControllerDelegate,
UIImagePickerControllerDelegate, UIActionSheetDelegate>
{
    
    IBOutlet UIImageView *userProfileImgView;
    
    IBOutlet UILabel *earnedPointLb;
    
    IBOutlet UITextField *firstNameTf;
    
     IBOutlet UITextField *lastNameTf;
    
    IBOutlet UITextField *licenceTf;
    
    IBOutlet UITextField *emailTf;
    
    IBOutlet UITextField *agencyTf;
    
    IBOutlet UITextField *phoneTf;
    
    IBOutlet UITextField *divisionTf;
    
    IBOutlet UITextField *subdivisionTf;
    
    IBOutlet UIButton *englishBtn, *chineeseBtn, *bahasaBtn, *tamilBtn;
    
    IBOutlet UIButton *updateButton;
    
    IBOutlet UIButton *loadRequestButton;

    
    
    
    
    
}

- (IBAction)openDrawer;

-(IBAction)editProfileAction:(id)sender;

@end
