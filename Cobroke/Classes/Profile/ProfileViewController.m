//
//  ProfileViewController.m
//  Cobroke
//
//  Created by Vikrant on 25/04/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "ProfileViewController.h"
#import "ServiceHelper.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "PhoneNumberViewController.h"
@interface ProfileViewController ()
{
    
    NSMutableArray *universalArray;
    
    
    
    NSInteger *divisionIndex;
    
    NSUInteger *subdivisionIndex;
    
    // textfield type
    NSString *tftype;
    
    UIPickerView *pickerView;
    
    
    NSArray *tempArray;
    
    
    NSString *agencyLicNumber;
    
    NSString *divisionID;
    
    NSString *subDivisionID;
    
    // save image url
    NSString *imageURL;
    
    
    NSString *langSelected;
    
    NSString *langEnglish;
    
    // to store selected Language
    NSString *langChinese;
    // to store selected Language
    NSString *langBahasa;
    // to store selected Language
    NSString *langTamil;

    
    // To check image is select from Gallery or not
    BOOL isImageSelected;
    
    NSDictionary *responseDict;
    
    BOOL IsScroll;
    
    BOOL isdoneAction;
    
    
    
}
@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    universalArray = [[NSMutableArray alloc] init];

    // For Initial
    isImageSelected = false;
    
    isdoneAction = false;
    
    [self setEnableDisableView:false];
    
    agencyTf.userInteractionEnabled = false;
    
    divisionTf.userInteractionEnabled = false;
    
    subdivisionTf.userInteractionEnabled = false;
    
    licenceTf.userInteractionEnabled = false;
    
    pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    
    [pickerView setDataSource: self];
    
    [pickerView setDelegate: self];
    
    pickerView.showsSelectionIndicator = YES;
    
    agencyTf.inputView = pickerView;
    [agencyTf addDoneOnKeyboardWithTarget:self action:@selector(agencyDoneAction:)];
    
    
    divisionTf.inputView = pickerView;
    [divisionTf addDoneOnKeyboardWithTarget:self action:@selector(divisionDoneAction:)];
    
    
    subdivisionTf.inputView = pickerView;
    [subdivisionTf addDoneOnKeyboardWithTarget:self action:@selector(subDivisionDoneAction:)];
    

    


    [self setupView];
    [self getProfileDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openDrawer
{
    [[AppDelegate globalDelegate] toggleLeftDrawer:self animated:YES];
}
#pragma mark -- agencyDoneAction
-(void)agencyDoneAction:(UIBarButtonItem*)button
{
    if ([universalArray count] > 0 )
    {
        if (!IsScroll && agencyTf.text.length == 0)
        {
            agencyTf.text = [[universalArray valueForKey:@"name"] firstObject];

        }
    }
    [self.view endEditing:YES];
    
    IsScroll = false;
    
}

#pragma mark -- divisionDoneAction
-(void)divisionDoneAction:(UIBarButtonItem*)button
{
    if ([[[universalArray firstObject] valueForKey:@"division"] count] > 0)
    {
        if (!IsScroll && divisionTf.text.length == 0)
        {
            divisionTf.text = [[[[universalArray firstObject] valueForKey:@"division"] valueForKey:@"divisionname"] firstObject];
        }
    }
    
    IsScroll = false;

    [self.view endEditing:YES];
    
}

#pragma mark -- subDivisionDoneAction
-(void)subDivisionDoneAction:(UIBarButtonItem*)button
{
    if  ([[[universalArray firstObject] valueForKey:@"subdivisions"] count] > 0)
    {
        if (!IsScroll && subdivisionTf.text.length == 0)
        {
            subdivisionTf.text = [[[[universalArray firstObject] valueForKey:@"subdivisions"] valueForKey:@"subdivision"]firstObject];
        }
    }
    
    IsScroll = false;

    [self.view endEditing:YES];
    
}
#pragma mark - ActionSheet delegates

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( buttonIndex == 0 ) {
        
        // if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *imagePickerView =[[UIImagePickerController alloc]init];
        
        imagePickerView.allowsEditing = YES;
        
        imagePickerView.delegate = self;
        
        [imagePickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
        
        if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self presentViewController:imagePickerView animated:YES completion:nil];
            }];
            
        }
        else{
            
            [self presentViewController:imagePickerView animated:NO completion:nil];
        }
        
        
    }else if( buttonIndex == 1 ) {
        
        UIImagePickerController *imagePickerView = [[UIImagePickerController alloc] init];
        
        imagePickerView.allowsEditing = YES;
        
        imagePickerView.delegate = self;
        
        imagePickerView.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self presentViewController:imagePickerView animated:YES completion:nil];
            }];
            
        }
        else
        {
            
            [self presentViewController:imagePickerView animated:NO completion:nil];
        }
        
        
    }
}
#pragma mark - PickerDelegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage * img = [info valueForKey:UIImagePickerControllerEditedImage];
    
    NSData *imgData= UIImageJPEGRepresentation(img,0.5 /*compressionQuality*/);
    
    
    UIImage *image=[UIImage imageWithData:imgData];
    
    userProfileImgView.image = image;
    
    // set Bool for image picker
    isImageSelected = true;
    
}
#pragma mark -- Get profile detail
-(void)getProfileDetails
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    
    NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    NSDictionary *param = @{@"oauth":oauthToken };
    
   // NSDictionary *param = @{@"oauth":@"87a42bd32658c87d2530351199df24c2",@"projectid": @"72" };
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"profiledetail" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         if (result)
         {
             
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 responseDict = result;
                 
                 firstNameTf.text = [result valueForKey:@"firstname"];
                 
                 lastNameTf.text = [result valueForKey:@"lastname"];
                 
                 licenceTf.text = [result valueForKey:@"agencylicense"];
                 
                 agencyTf.text = [result valueForKey:@"agencyname"];
                 
                 emailTf.text = [result valueForKey:@"email"];
                 
                 phoneTf.text = [result valueForKey:@"phoneno"];
                 
                 divisionTf.text = [result valueForKey:@"division"];
                 
                 subdivisionTf.text = [result valueForKey:@"subdivision"];
                 
                 
                 agencyLicNumber = [result valueForKey:@"agencylicense"];
                 
                 divisionID = [result valueForKey:@"divisionid"];
                 
                 subDivisionID = [result valueForKey:@"subdivisionid"];
                 
                 imageURL = [result valueForKey:@"profilepic"];
                 
                 NSArray *langArray = [responseDict valueForKey:@"languages"];
                 
                 for (int i = 0; i < langArray.count; i++)
                 {
                     
                     if( [englishBtn.titleLabel.text caseInsensitiveCompare:[langArray objectAtIndex:i]] == NSOrderedSame )
                     {
                         NSLog(@"string matched without case");
                         
                         englishBtn.selected = YES;
                         
                        [self changeButtonColor:englishBtn isSelectes:YES];
                         
                         langEnglish = @"english";
                         
                         
                     }
                     
                     else if ( [tamilBtn.titleLabel.text caseInsensitiveCompare:[langArray objectAtIndex:i]] == NSOrderedSame )
                     {
                         tamilBtn.selected = YES;

                         [self changeButtonColor:tamilBtn isSelectes:YES];
                         langTamil = @"tamil";

                         
                     }
                     else if ( [bahasaBtn.titleLabel.text caseInsensitiveCompare:[langArray objectAtIndex:i]] == NSOrderedSame )
                     {
                         
                         bahasaBtn.selected = YES;

                         [self changeButtonColor:bahasaBtn isSelectes:YES];
                         
                         langBahasa = @"bahasa";

                     }
                     else if ( [chineeseBtn.titleLabel.text caseInsensitiveCompare:[langArray objectAtIndex:i]] == NSOrderedSame )
                     {
                         
                         chineeseBtn.selected = YES;

                         [self changeButtonColor:chineeseBtn isSelectes:YES];
                         
                         langChinese = @"chinese";

                         
                     }
                 }
                 
                 
                

                 
                 
                 

                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                     [userProfileImgView sd_setImageWithURL:[result valueForKey:@"profilepic"] placeholderImage:[UIImage imageNamed:@"user_Dummy"]];
                     
                 });
                 
             }
             else
             {
                 
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Title" message:[result valueForKey:@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                     
                     NSLog(@"go to home page");
                     if ([[result valueForKey:@"msg"] isEqualToString:@"Invalid Token."])
                     {
                         
                         
                         [[NSUserDefaults standardUserDefaults] setObject:@"logout" forKey:@"isLogin"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"authToken"];
                         
                         
                         PhoneNumberViewController *pVc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
                         
                         [self.navigationController pushViewController:pVc animated:YES];
                         
                         
                     }
                     
                 }]];
                 
                 [self presentViewController:alertController animated:YES completion:nil];
                 
             }
             
             
             
             
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
}
#pragma mark - Textfield Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == firstNameTf)
    {
        [licenceTf becomeFirstResponder];
    }
    else if (textField == licenceTf)
    {
        [agencyTf becomeFirstResponder];
    }
    
    else if (textField == agencyTf)
    {
        [emailTf becomeFirstResponder];
    }
    
    else if (textField == emailTf)
    {
        [phoneTf becomeFirstResponder];
    }
    
    else if (textField == phoneTf)
    {
        [divisionTf becomeFirstResponder];
    }
    
    else if (textField == divisionTf)
    {
        [divisionTf resignFirstResponder];
    }
    
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == agencyTf)
    {
        tftype = @"agency";
        
        universalArray = [[responseDict valueForKey:@"agencies"] mutableCopy];
        
        
        return YES;
    }
    else if(textField == divisionTf)
    {
        if (agencyTf.text.length == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Select Agency " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
            return NO;
        }
        else
        {
            tftype = @"division";
            
            [universalArray removeAllObjects];
            
            universalArray = [[self getDivision:agencyTf.text] mutableCopy];
            
            [pickerView reloadAllComponents];
            
            
            return YES;
        }
    }
    else if (textField == subdivisionTf)
    {
        
        tftype = @"subdivision";
        
        if (divisionTf.text.length == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Select Division" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
            return NO;
        }
        else
        {
            
            NSArray *filterArray = [self getsubDivision:divisionTf.text tofileterArray:universalArray];
            
            universalArray = [NSMutableArray new];
            
            universalArray = [filterArray mutableCopy];
            
            [pickerView reloadAllComponents];
            
            return YES;
        }
        
    }
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    
    
}

#pragma mark - PickerView delegates
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
}
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if ([tftype isEqualToString:@"agency"])
    {
        return [universalArray count];
    }
    else if ([tftype isEqualToString:@"division"])
    {
        return [[[universalArray firstObject] valueForKey:@"division"] count];
    }
    else
    {
        return [[[universalArray firstObject] valueForKey:@"subdivisions"] count];
    }
}


- (NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    NSString *agencyName;
    
    if ([tftype isEqualToString:@"agency"])
    {
        
        agencyName = [[universalArray valueForKey:@"name"] objectAtIndex:row];
        
    }
    else if ([tftype isEqualToString:@"division"])
    {
        agencyName = [[[[universalArray objectAtIndex:0] objectForKey:@"division"] objectAtIndex:row] objectForKey:@"divisionname"];
    }
    else
    {
        
        agencyName = [[[[universalArray firstObject] valueForKey:@"subdivisions"] valueForKey:@"subdivision"]objectAtIndex:row];
        
    }
    
    
    
    return agencyName;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (universalArray != nil) {
        if ([tftype isEqualToString:@"agency"])
        {
            
            agencyTf.text = [[universalArray valueForKey:@"name"] objectAtIndex:row];
            
            agencyLicNumber = [[universalArray valueForKey:@"agencylicenseno"] objectAtIndex:row];
            
            divisionTf.text = @"";
            subdivisionTf.text = @"";
            
        }
        else if ([tftype isEqualToString:@"division"])
        {
            divisionTf.text = [[[[universalArray firstObject] valueForKey:@"division"] valueForKey:@"divisionname"] objectAtIndex:row];
            
            divisionID = [[[[universalArray firstObject] valueForKey:@"division"] valueForKey:@"divisionid"] objectAtIndex:row];
            
            subdivisionTf.text = @"";

            
        }
        else
        {
            subdivisionTf.text = [[[[universalArray firstObject] valueForKey:@"subdivisions"] valueForKey:@"subdivision"]objectAtIndex:row];
            
            subDivisionID = [[[[universalArray firstObject] valueForKey:@"subdivisions"] valueForKey:@"subdivisionid"]objectAtIndex:row];
        }
    }
    
    IsScroll = true;

}

#pragma mark -- Get profile detail
-(void)updateProfile
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    
    NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    
    NSString *imageStr;
    
    if (isImageSelected)
    {
        imageStr = [self imageToBase64:userProfileImgView.image];
        
    }
    else
    {
        imageStr = imageURL;
    }
    
    
   NSDictionary *param = @{@"oauth":oauthToken,@"firstname":firstNameTf.text,@"lastname":@"Tanwar",@"cealicense":licenceTf.text,@"agency":agencyTf.text,@"email":emailTf.text,@"division":divisionTf.text,@"subdivision":@"",@"profilepic":imageStr,@"language":@"Hindi"};
    
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"updateprofile" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         if (result)
         {
             
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 firstNameTf.text = [result valueForKey:@"firstname"];
                 
                 licenceTf.text = [result valueForKey:@"agencylicense"];
                 
                 agencyTf.text = [result valueForKey:@"agencyname"];
                 
                 emailTf.text = [result valueForKey:@"email"];
                 
                 phoneTf.text = [result valueForKey:@"phoneno"];
                 
                 //divisionTf.text = [result valueForKey:@"division"];
                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                     [userProfileImgView sd_setImageWithURL:[result valueForKey:@"profilepic"] placeholderImage:[UIImage imageNamed:@"user_Dummy"]];
                     
                 });
                 
             }
             else
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 
                 [alert show];
             }
             
             
             
             
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
}

#pragma mark -- Edit Button Action
-(IBAction)editProfileAction:(id)sender

{
    
    if (isdoneAction)
    {
        isdoneAction = false;
        
        [self setEnableDisableView:false];
        
        [updateButton setImage:[UIImage imageNamed:@"Edit"] forState:UIControlStateNormal];
        
        [updateButton setTitle:@"" forState:UIControlStateNormal];
        
        [self updateUserData];

    }
    else
    {
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        
        [userProfileImgView addGestureRecognizer:tapGestureRecognizer];
        
        
        
        [self setEnableDisableView:true];
        
        
        [updateButton setImage:nil forState:UIControlStateNormal];
        
        [updateButton setTitle:@"Submit" forState:UIControlStateNormal];
        
        isdoneAction = true;
    }
    
    
}

#pragma mark --  image to base 64 --
-(NSString*)imageToBase64:(UIImage*)image{
    
    NSData *compressedData = UIImageJPEGRepresentation(image, .5);
    
    NSString *base64Str = [compressedData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    
    //return @"dfjdfjkljfdfjkjkfjkjkljfkljdl;f";
    return base64Str;
}

#pragma mark - Void


- (void)setupView
{
    self.navigationController.navigationBar.hidden = YES;
    
    [englishBtn setTitle:@"English" forState:UIControlStateNormal];
    
    [self changeButtonColor:englishBtn isSelectes:NO];
    
    //
    
    [chineeseBtn setTitle:@"Chinese" forState:UIControlStateNormal];
    
    [self changeButtonColor:chineeseBtn isSelectes:NO];
    
    //
    
    [bahasaBtn setTitle:@"Bahasa" forState:UIControlStateNormal];
    
    [self changeButtonColor:bahasaBtn isSelectes:NO];
    
    //
    
    [tamilBtn setTitle:@"Tamil" forState:UIControlStateNormal];
    
    [self changeButtonColor:tamilBtn isSelectes:NO];
    
    

    
    
}
#pragma mark - Action

- (IBAction)languageSelection:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.tag == 1)
    {
        if (sender.selected == YES)
        {
            langEnglish = @"english";
            
        }
        else
        {
            langEnglish = @"";
        }
        
    }
    else if (sender.tag == 2)
    {
        if (sender.selected == YES)
        {
            langChinese = @"chinese";
        }
        else
        {
            langChinese = @"";
        }
    }
    else if (sender.tag == 3)
    {
        if (sender.selected == YES)
        {
            langBahasa = @"bahasa";
        }
        else
        {
            langBahasa = @"";
        }
    }
    else
    {
        if (sender.selected == YES)
        {
            langTamil = @"tamil";
        }
        else
        {
            langTamil = @"";
        }
    }
    if (sender.selected == YES)
    {
        [self changeButtonColor:sender isSelectes:YES];
    }
    else
    {
        [self changeButtonColor:sender isSelectes:NO];
    }
}

#pragma mark - Change language button color

- (void)changeButtonColor:(UIButton *)button isSelectes:(BOOL)isSelect
{
    if (isSelect == YES)
    {
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        button.backgroundColor = [UIColor blackColor];
        
        button.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    else
    {
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        button.backgroundColor = [UIColor whiteColor];
        
        button.layer.borderColor = [UIColor blackColor].CGColor;
    }
    
    button.layer.borderWidth = 1.0;
    
    button.layer.cornerRadius = englishBtn.frame.size.height/2;
}

#pragma mark --  GetDivision

-(NSArray*)getDivision:(NSString*)agencyName
{
    NSArray *localArr = [responseDict objectForKey:@"agencies"];
    
    NSPredicate *perdicate = [NSPredicate predicateWithFormat:@"name == %@",agencyName];
    
    NSArray *filterArray = [localArr filteredArrayUsingPredicate:perdicate];
    
    // [self getsubDivision:@"ANC" tofileterArray:filterArray];
    
    return filterArray;
}

-(NSArray*)getsubDivision:(NSString*)divisionName tofileterArray:(NSArray*)array
{
    
    NSSet *set1 = [NSSet setWithArray:tempArray];
    NSSet *set2 = [NSSet setWithArray:array];
    
    if (![set1 isEqualToSet:set2]) {
        
        NSLog(@"same array");
        NSArray *localArr = [[array valueForKey:@"division"] firstObject];
        
        NSPredicate *perdicate = [NSPredicate predicateWithFormat:@"divisionname == %@",divisionName];
        
        NSArray *filterArray = [localArr filteredArrayUsingPredicate:perdicate];
        
        tempArray = filterArray;
        
        return filterArray;
    }
    return array;
    
}

#pragma mark -- Gesture Action
- (void)tapGestureAction:(UITapGestureRecognizer *)gestureRecognizer{
    
    
    
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Select image from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"From library",@"From camera", nil];
    
    [action showInView:self.view];
    
    
}

-(void)setEnableDisableView:(BOOL)action{
    
    
    
    IsScroll = action;
    
    firstNameTf.userInteractionEnabled = action;
    
    lastNameTf.userInteractionEnabled = action;
    
    emailTf.userInteractionEnabled = action;
    
    phoneTf.userInteractionEnabled = action;
    
    userProfileImgView.userInteractionEnabled = action;
    
    englishBtn.userInteractionEnabled  =action;
    
    tamilBtn.userInteractionEnabled = action;
    
    bahasaBtn.userInteractionEnabled = action;
    
    chineeseBtn.userInteractionEnabled = action;
    
    loadRequestButton.hidden = !action;

    

    
}




#pragma mark -- web service
-(void)updateUserData
{
    NSLog(@"already registered..  %@",[responseDict valueForKey:@"alreadyregistered"]);
    
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    
        if (langEnglish.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langEnglish;
            }
            else
            {
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langEnglish];
            }
            
        }
        if (langChinese.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langChinese;
            }
            else
            {
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langChinese];
                
            }
        }
        if (langBahasa.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langBahasa;
            }
            else
            {
                
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langBahasa];
                
            }
        }
        if (langTamil.length != 0) {
            
            if (langSelected == nil)
            {
                langSelected = langTamil;
            }
            else
            {
                langSelected = [NSString stringWithFormat:@"%@,%@",langSelected,langTamil];
                
            }
        }
        
        if (langSelected.length == 0)
        {
            langSelected = @"";
        }
        if (agencyLicNumber == nil)
        {
            agencyLicNumber = @"";
        }
        if (divisionID ==  nil)
        {
            divisionID = @"";
        }
        if (subDivisionID == nil)
        {
            subDivisionID = @"";
        }
        NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
        
        NSString *imageStr;
        
        if (isImageSelected)
        {
            imageStr = [self imageToBase64:userProfileImgView.image];
            
        }
        else
        {
            imageStr = [responseDict valueForKey:@"profilepic"];
        }
        
        
        NSDictionary *param = @{@"oauth":oauthToken,@"firstname":firstNameTf.text,@"lastname":lastNameTf.text,@"cealicense":licenceTf.text,@"agency":agencyLicNumber,@"email":emailTf.text,@"division":divisionID,@"subdivision":subDivisionID,@"profilepic":imageStr,@"language":langSelected};
    
        NSLog(@"dict send  %@",param);
        
        [[ServiceHelper sharedManager] sendRequestWithMethodName:@"updateprofile" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"response ...  %@",result);
             
             if (result)
             {
                 NSString *status = [result valueForKey:@"status"];
                 if ([status isEqualToString:@"true"])
                 {
                    
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     
                     [alert show];
                 }
                 
             }
             else
             {
                 [[ServiceHelper sharedManager] setHudShow:NO];
                 NSLog(@"Login unsuccess.... ");
                 
             }
             
         }];
    
}

-(IBAction)loadRequestButtonAction:(id)sender{
    
    
    
    [SVProgressHUD show];
    
    [self performSelector:@selector(closeloading) withObject:nil afterDelay:2.0];
  
    
    
    
    
}

-(void)closeloading
{
    [SVProgressHUD dismiss];
    
    agencyTf.userInteractionEnabled = YES;
    
    divisionTf.userInteractionEnabled = YES;
    
    subdivisionTf.userInteractionEnabled = YES;
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
