//
//  LeftDrawerViewController.m
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//


#import "LeftDrawerViewController.h"
#import "NotificationViewController.h"
#import "ProjectsViewController.h"
#import "JVFloatingDrawerViewController.h"
#import "JVFloatingDrawerSpringAnimator.h"
#import "ProfileViewController.h"
#import "PhoneNumberViewController.h"

@interface LeftDrawerViewController ()
{
    NSArray *dataArr;
}
@end


@implementation LeftDrawerViewController



#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}



#pragma mark - Void


- (void)setupView
{
    dataArr = [[NSArray alloc] initWithObjects:@"Home",@"Profile", @"Notifications", @"About", @"Logout", nil];
}


#pragma mark - Table View Delegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
   
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    cell.textLabel.text = [dataArr objectAtIndex:indexPath.row];
    
    cell.textLabel.font = [UIFont fontWithName:@"CircularStd-Medium" size:15];
    
    cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0)
    {
        HomeViewController *nVc = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        
        [[(APPDEL) drawerViewController] setCenterViewController:nVc];
        
        [(APPDEL) toggleLeftDrawer:self animated:YES];
        
        
    }
    
    else if (indexPath.row == 1)
    {
        
        
        ProfileViewController *pVc = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
        
        [[(APPDEL) drawerViewController] setCenterViewController:pVc];
        
        [(APPDEL) toggleLeftDrawer:self animated:YES];

    }
    
    else if (indexPath.row == 2)
    {
        NotificationViewController *nVc = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
        
        [[(APPDEL) drawerViewController] setCenterViewController:nVc];
        
        [(APPDEL) toggleLeftDrawer:self animated:YES];
    }
    else if (indexPath.row == 4)
    {
        // Login Session
        [[NSUserDefaults standardUserDefaults] setObject:@"logout" forKey:@"isLogin"];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"authToken"];

        
        PhoneNumberViewController *pVc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
        
        [self.navigationController pushViewController:pVc animated:YES];

    }

    
}


@end
