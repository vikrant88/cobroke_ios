//
//  LeftDrawerViewController.h
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftDrawerViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *dataTbl;
}


@end
