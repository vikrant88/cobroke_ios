//
//  AgencyRequestVC.h
//  Cobroke
//
//  Created by Vikrant on 25/04/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgencyRequestVC : UIViewController
{
    
    IBOutlet UILabel *label;
    
}

@property(strong, nonatomic) NSString *labelText;

-(IBAction)continueButtonAction:(id)sender;
@end
