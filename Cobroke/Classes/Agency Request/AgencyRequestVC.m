//
//  AgencyRequestVC.m
//  Cobroke
//
//  Created by Vikrant on 25/04/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "AgencyRequestVC.h"
#import "JVFloatingDrawerViewController.h"
#import "JVFloatingDrawerSpringAnimator.h"
#import "PhoneNumberViewController.h"

@interface AgencyRequestVC ()

@end

@implementation AgencyRequestVC
@synthesize labelText;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    label.text = labelText;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Mark -- Continue Button Action
-(IBAction)continueButtonAction:(id)sender
{
    
    NSArray *array = self.navigationController.viewControllers;
    
    if ([[array objectAtIndex:array.count-2] isKindOfClass:[RegisterViewController class]]) {
        NSLog(@"register vc");
        
        PhoneNumberViewController *pVc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
        
        [self.navigationController pushViewController:pVc animated:NO];

        
//         [self.navigationController popToViewController:[array objectAtIndex:array.count-2] animated:YES];
    }
    else
    {
        JVFloatingDrawerViewController *controller = [AppDelegate globalDelegate].drawerViewController;
    
        [self.navigationController popToViewController:controller animated:YES];
    
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
