//
//  AssignTaggerViewController.h
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignTaggerViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *assignTagTbl;
    
    NSMutableArray *agentsArray;
}

@property(assign, nonatomic)NSUInteger projectID;


- (IBAction)backTap;


@end
