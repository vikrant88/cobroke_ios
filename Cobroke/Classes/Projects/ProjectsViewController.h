//
//  ProjectsViewController.h
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectsViewController : UIViewController
{
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UIButton *rqNowButton;
    
    IBOutlet UIButton *rqLaterButton;
    
    IBOutlet UIButton *assignTaggerButton;
    
    IBOutlet UIImageView *projectImgView;
    
    IBOutlet UILabel *projectLocationLb;
    
    IBOutlet UILabel *projectTimingLb;
    
    IBOutlet UILabel *phoneNumberLb;
    
    IBOutlet UILabel *projectNameLb;
    
//    IBOutlet UIImageView *agencyImgView1;
//    
//    IBOutlet UIImageView *agencyImgView2;
    
    IBOutlet UIView *logoBgView;
    
    
    
    
}

- (IBAction)nextScreen;

-(IBAction)requestLaterAction:(id)sender;

-(IBAction)requestNowAction:(id)sender;


- (IBAction)openDrawer;

@property(assign, nonatomic)NSUInteger projectID;
@property(assign, nonatomic)NSString  *projectRole;


@end
