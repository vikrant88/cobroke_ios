//
//  AssignTaggerViewController.m
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "AssignTaggerViewController.h"
#import "TaggerTableViewCell.h"
#import "ServiceHelper.h"

@interface AssignTaggerViewController ()
{
    NSMutableArray *peopleArr;
}

@end


@implementation AssignTaggerViewController
@synthesize projectID;

#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupView];
    
    [self getAllTaggers];
    
    agentsArray = [[NSMutableArray alloc]init];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}



#pragma mark - Void


- (void)setupView
{
    self.navigationController.navigationBar.hidden = YES;
    
    
    peopleArr = [[NSMutableArray alloc] initWithObjects:@{@"name" : @"Jaskirat", @"phone" : @"+1 123456789", @"isTagger" : @"0"}, @{@"name" : @"Jaskirat", @"phone" : @"+1 123456789", @"isTagger" : @"0"}, @{@"name" : @"Jaskirat", @"phone" : @"+1 123456789", @"isTagger" : @"0"}, @{@"name" : @"Jaskirat", @"phone" : @"+1 123456789", @"isTagger" : @"0"}, @{@"name" : @"Jaskirat", @"phone" : @"+1 123456789", @"isTagger" : @"0"}, @{@"name" : @"Jaskirat", @"phone" : @"+1 123456789", @"isTagger" : @"0"}, @{@"name" : @"Jaskirat", @"phone" : @"+1 123456789", @"isTagger" : @"0"}, @{@"name" : @"Jaskirat", @"phone" : @"+1 123456789", @"isTagger" : @"0"}, @{@"name" : @"Jaskirat", @"phone" : @"+1 123456789", @"isTagger" : @"0"}, nil];
}


#pragma mark - Action

- (void)backTap
{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - Table View Delegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return agentsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"CELLID";
    
    TaggerTableViewCell *cell = (TaggerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
   
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TaggerTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.nameLbl.text = [[agentsArray valueForKey:@"firstname"] objectAtIndex:indexPath.row];
   
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
         [cell.userImg sd_setImageWithURL:[[agentsArray valueForKey:@"agentimages"] objectAtIndex:indexPath.row] placeholderImage:[UIImage imageNamed:@"user_Dummy"]];
            
        });
    
    if ([[[agentsArray objectAtIndex:indexPath.row]objectForKey:@"isSelected"] isEqualToString:@"yes"])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;

    }
    
    cell.phoneLbl.text = [[agentsArray valueForKey:@"phoneno"] objectAtIndex:indexPath.row];
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    dict = [agentsArray objectAtIndex:indexPath.row] ;
    
     if ([[[agentsArray objectAtIndex:indexPath.row]objectForKey:@"isSelected"] isEqualToString:@"yes"])
     {
         [dict setObject:@"no" forKey:@"isSelected"];

     }
    else
    {
        [dict setObject:@"yes" forKey:@"isSelected"];

    }
    
    
    [agentsArray replaceObjectAtIndex:indexPath.row withObject:dict];
    
    [tableView reloadData];
    
}
#pragma mark -- Get Single Project
-(void)getAllTaggers
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    
    NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    
//    NSDictionary *param = @{@"oauth":oauthToken,@"projectid": [NSString stringWithFormat:@"%lu",(unsigned long)projectID] };
    
        NSDictionary *param = @{@"oauth":oauthToken,@"projectid": [NSString stringWithFormat:@"%lu",(unsigned long)projectID] };
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"taggerlist" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         NSMutableArray *mutableArray = [NSMutableArray new];
         
         
         
         if (result)
         {
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 
                 for (int i =0 ; i<[[result valueForKey:@"allagents"] count]; i++)
                 {
                     
                     NSMutableDictionary *dict = [[[result valueForKey:@"allagents"] objectAtIndex:i] mutableCopy];
                     
                     [dict setObject:@"no" forKey:@"isSelected"];
                     
                     
                     [mutableArray addObject:dict];
                     
                     
                 }
                 NSLog(@"edited mutable array .. %@",mutableArray);

                 agentsArray = [mutableArray mutableCopy];
                 
                 [assignTagTbl reloadData];
                 
             }
             else
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 
                 [alert show];
             }
             
             
             
             
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
}

#pragma mark -- done button Action

-(IBAction)doneButtonAction:(id)sender
{
    NSMutableArray  *array = [NSMutableArray new];
    
    for (int i = 0 ; i< agentsArray.count; i++)
    {
        if ([[[agentsArray objectAtIndex:i]objectForKey:@"isSelected"] isEqualToString:@"yes"])
        {
            NSMutableDictionary *dict = [NSMutableDictionary new];
            
            [dict setObject:[[agentsArray objectAtIndex:i] valueForKey:@"id"] forKey:@"id"];
            
            if ([[[agentsArray objectAtIndex:i] valueForKey:@"flag"] integerValue])
            {
                 [dict setObject:@"1" forKey:@"flag"];
            }
            else
            {
                [dict setObject:@"0" forKey:@"flag"];

            }
            
//            if ([[[agentsArray objectAtIndex:i] valueForKey:@"role"] isEqualToString:@"Tagger"])
//            {
//                 [dict setObject:@"1" forKey:@"flag"];
//            }
//            else
//            {
//                 [dict setObject:@"0" forKey:@"flag"];
//            }
            [array addObject: dict];
        }
    }
    
   NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:0 error:nil];
    
    
    NSString *jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:nil];
    
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"nsjson....  %@",jsonString);
 
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    
    //    NSDictionary *param = @{@"oauth":oauthToken,@"projectid": [NSString stringWithFormat:@"%lu",(unsigned long)projectID] };
    
    NSDictionary *param = @{@"oauth":oauthToken,@"projectid": [NSString stringWithFormat:@"%lu",(unsigned long)projectID] ,@"taggers":jsonString };
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"taggernotification" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         NSMutableArray *mutableArray = [NSMutableArray new];
         
         if (result)
         {
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 
                 [alert show];
                 
             }
             else
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 
                 [alert show];
             }
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];

    
}
@end
