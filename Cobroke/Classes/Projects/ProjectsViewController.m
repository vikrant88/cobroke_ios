//
//  ProjectsViewController.m
//  Cobroke
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 4/19/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//


#import "ProjectsViewController.h"
#import "AssignTaggerViewController.h"
#import "RequestAppointmentVC.h"
#import "AgencyRequestVC.h"
#import "ServiceHelper.h"
#import "AllAgentVC.h"
#import "JVFloatingDrawerViewController.h"

@interface ProjectsViewController ()
{
    
    NSDictionary *responseDict;
}

@end


@implementation ProjectsViewController
@synthesize  projectRole,projectID;

#pragma mark - ViewCycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([projectRole isEqualToString:@"MIC"])
    {
        assignTaggerButton.hidden = NO ;
        
    }
    else
    {
        assignTaggerButton.hidden = YES ;
        
        rqNowButton.hidden = NO;
        
        rqLaterButton.hidden = NO;

    }
    
//    if (self.viewTag  == 1)
//    {
//        assignTaggerButton.hidden = YES ;
//        
//        rqNowButton.hidden = NO;
//        
//        rqLaterButton.hidden = NO;
//        
//    }
//    else if (self.viewTag == 2)
//    {
//        assignTaggerButton.hidden = NO ;
//        
//        rqNowButton.hidden = YES;
//        
//        rqLaterButton.hidden = YES;
//    }
//    else if (self.viewTag == 3)
//    {
//        assignTaggerButton.hidden = YES ;
//        
//        rqNowButton.hidden = NO;
//        
//        rqLaterButton.hidden = NO;
//        
//        assignTaggerButton.hidden = YES ;
//        
//        rqNowButton.hidden = NO;
//        
//        rqLaterButton.hidden = NO;
//
//    }
//    else
//    {
//        
//        assignTaggerButton.hidden = NO ;
//        
//        rqNowButton.hidden = YES;
//        
//        rqLaterButton.hidden = YES;
//    }
    
    [self setupView];
    [self getProjectDetails];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
   
    [super viewDidAppear:animated];

    if ([[responseDict valueForKey:@"agencies"] count] == 1) {
        
        UIButton *button = [[UIButton alloc]init];
        
        [self customeLayer:button];

        button.tag = 0;
        
        button.layer.masksToBounds = YES;
        
        button.frame = CGRectMake(logoBgView.frame.size.width/2 - 30, 5, 60, 60);
        
        button.backgroundColor = [UIColor lightGrayColor];
        
        [self setdownloadedImage:button downloadFromURl:[[[responseDict valueForKey:@"agencies"] valueForKey:@"agencylogo"] objectAtIndex:0]];
        
        [button addTarget:self action:@selector(buttonSubmit:) forControlEvents:UIControlEventTouchUpInside];
        
        [self customeLayer:button];

        [logoBgView addSubview:button];
        
        
    }
    else if([[responseDict valueForKey:@"agencies"] count] == 2)
    {
        UIButton *button = [[UIButton alloc]init];
        
        
        [self customeLayer:button];
        
         button.tag = 0;
        
         [button addTarget:self action:@selector(buttonSubmit:) forControlEvents:UIControlEventTouchUpInside];
        
         [self setdownloadedImage:button downloadFromURl:[[[responseDict valueForKey:@"agencies"] valueForKey:@"agencylogo"] objectAtIndex:0]];

        button.frame = CGRectMake(logoBgView.frame.size.width/2 - 70, 5, 60, 60);
        
        button.backgroundColor = [UIColor lightGrayColor];
        
        [logoBgView addSubview:button];
        
        UIButton *button1 = [[UIButton alloc]init];
        
        [self setdownloadedImage:button1 downloadFromURl:[[[responseDict valueForKey:@"agencies"] valueForKey:@"agencylogo"] objectAtIndex:1]];
        [self customeLayer:button1];
        
         button1.tag = 1;
        
         [button1 addTarget:self action:@selector(buttonSubmit:) forControlEvents:UIControlEventTouchUpInside];
        
        button1.frame = CGRectMake(CGRectGetMaxX(button.frame)+20, 5, 60, 60);

        button1.backgroundColor = [UIColor lightGrayColor];
        
        [logoBgView addSubview:button1];
        
        
    }
    else if([[responseDict valueForKey:@"agencies"] count] == 3)
    {
        UIButton *button = [[UIButton alloc]init];
        
        [self customeLayer:button];
        
         [button addTarget:self action:@selector(buttonSubmit:) forControlEvents:UIControlEventTouchUpInside];
        
        button.frame = CGRectMake(logoBgView.frame.size.width/2 - 110, 5, 60, 60);
        
        button.tag = 0;
        
        button.backgroundColor = [UIColor lightGrayColor];
        
        [self setdownloadedImage:button downloadFromURl:[[[responseDict valueForKey:@"agencies"] valueForKey:@"agencylogo"] objectAtIndex:0]];
        
        [logoBgView addSubview:button];
        
        UIButton *button1 = [[UIButton alloc]init];
        
        [self customeLayer:button1];
        
        button1.tag = 1;
        
        button1.frame = CGRectMake(CGRectGetMaxX(button.frame)+20, 5, 60, 60);
        
         [button1 addTarget:self action:@selector(buttonSubmit:) forControlEvents:UIControlEventTouchUpInside];
        
        button1.backgroundColor = [UIColor lightGrayColor];
        
        [self setdownloadedImage:button1 downloadFromURl:[[[responseDict valueForKey:@"agencies"] valueForKey:@"agencylogo"] objectAtIndex:1]];
        
        [logoBgView addSubview:button1];
        
        UIButton *button2 = [[UIButton alloc]init];
        
        [self customeLayer:button2];
        
        button2.frame = CGRectMake(CGRectGetMaxX(button1.frame)+20, 5, 60, 60);
        
         [button2 addTarget:self action:@selector(buttonSubmit:) forControlEvents:UIControlEventTouchUpInside];
        
        button2.tag = 2;
        
        button2.backgroundColor = [UIColor lightGrayColor];
        
        [logoBgView addSubview:button2];
        
    }
    else if([[responseDict valueForKey:@"agencies"] count] == 4)
    {
        UIButton *button = [[UIButton alloc]init];
        
        [self customeLayer:button];
        
         [button addTarget:self action:@selector(buttonSubmit:) forControlEvents:UIControlEventTouchUpInside];
        
        button.frame = CGRectMake(logoBgView.frame.size.width/2 - 150, 5, 60, 60);
        
         button.tag = 0;
        
        [self setdownloadedImage:button downloadFromURl:[[[responseDict valueForKey:@"agencies"] valueForKey:@"agencylogo"] objectAtIndex:0]];
        
        button.backgroundColor = [UIColor lightGrayColor];
        
        [logoBgView addSubview:button];
        
        
        UIButton *button1 = [[UIButton alloc]init];
        
        [self customeLayer:button1];
        
        button1.frame = CGRectMake(CGRectGetMaxX(button.frame)+20, 5, 60, 60);
        
        button1.backgroundColor = [UIColor lightGrayColor];
        
         button1.tag = 1;
        
         [self setdownloadedImage:button1 downloadFromURl:[[[responseDict valueForKey:@"agencies"] valueForKey:@"agencylogo"] objectAtIndex:1]];
        
         [button1 addTarget:self action:@selector(buttonSubmit:) forControlEvents:UIControlEventTouchUpInside];
        
        [logoBgView addSubview:button1];
        
        
        UIButton *button2 = [[UIButton alloc]init];
        
        [self customeLayer:button2];
        
        button2.frame = CGRectMake(CGRectGetMaxX(button1.frame)+20, 5, 60, 60);
        
        [button2 addTarget:self action:@selector(buttonSubmit:) forControlEvents:UIControlEventTouchUpInside];
        
         button2.tag = 2;
        
          [self setdownloadedImage:button2 downloadFromURl:[[[responseDict valueForKey:@"agencies"] valueForKey:@"agencylogo"] objectAtIndex:2]];
        
        button2.backgroundColor = [UIColor lightGrayColor];
        
        [logoBgView addSubview:button2];
        
        
        UIButton *button3 = [[UIButton alloc]init];
        
        [self customeLayer:button3];
        
         [button3 addTarget:self action:@selector(buttonSubmit:) forControlEvents:UIControlEventTouchUpInside];
        
         button3.tag = 3;
        
        [self setdownloadedImage:button3 downloadFromURl:[[[responseDict valueForKey:@"agencies"] valueForKey:@"agencylogo"] objectAtIndex:3]];
        button3.frame = CGRectMake(CGRectGetMaxX(button2.frame)+20, 5, 60, 60);
        
        button3.backgroundColor = [UIColor lightGrayColor];
        
        [logoBgView addSubview:button3];
        
        
    }
     });
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}



#pragma mark - Void


- (void)setupView
{
    self.navigationController.navigationBar.hidden = YES;
}


#pragma mark - Action

- (IBAction)nextScreen
{
    AssignTaggerViewController *vc = [[AssignTaggerViewController alloc] initWithNibName:@"AssignTaggerViewController" bundle:nil];
    
    vc.projectID = projectID;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- Request Later Action
-(IBAction)requestLaterAction:(id)sender
{
    
    RequestAppointmentVC *vc = [[RequestAppointmentVC alloc] initWithNibName:@"RequestAppointmentVC" bundle:nil];
    
    vc.projectID = projectID;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark -- Request now Action
-(IBAction)requestNowAction:(id)sender
{
    
     NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    NSDictionary *param = @{@"oauth":oauthToken,@"projectid": [NSString stringWithFormat:@"%lu",(unsigned long)projectID]};
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"requestnotification" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         NSLog(@"%@",self.navigationController.viewControllers);
         
//         for (UIViewController *controller in self.navigationController.viewControllers){
//             
//             if([controller isKindOfClass:[JVFloatingDrawerViewController class]])
//             {
//                 [self.navigationController popToViewController:controller animated:YES];
//                 
//             }
//             
//         }
         

         if (result)
         {
             
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 
                 
                 HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                 
                 
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 
                 [alert show];
                 
                 
             }
             else
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 
                 [alert show];
             }
             
             
             
             
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@" unsuccess.... ");
             
         }
         
     }];
    
    
    
    
    
//    AgencyRequestVC *vc = [[AgencyRequestVC alloc] initWithNibName:@"AgencyRequestVC" bundle:nil];
//     vc.labelText = @"Your request has been submitted.";
//    [self.navigationController pushViewController:vc animated:YES];
    
}


- (IBAction)openDrawer
{
   // [[AppDelegate globalDelegate] toggleLeftDrawer:self animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Get Single Project 
-(void)getProjectDetails
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    
    NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
    NSDictionary *param = @{@"oauth":oauthToken,@"projectid": [NSString stringWithFormat:@"%lu",(unsigned long)projectID] };
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"singleproject" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         NSLog(@"response ...  %@",result);
         
         if (result)
         {
             
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 responseDict = result;
                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                  
                     
                     [projectImgView sd_setImageWithURL:[result valueForKey:@"projectimages"] placeholderImage:[UIImage imageNamed:@"building.jpeg"]];
                     
                     CGFloat spacing = 20;
                     
//                     for (int i = 0; i < [[result valueForKey:@"agencies"] count]; i++)
//                     {
                     
                         
                     //}
                     
//                     [agencyImgView1 sd_setImageWithURL:[[[result valueForKey:@"agencies"] valueForKey:@"agencylogo"] firstObject] placeholderImage:[UIImage imageNamed:@"dummy_logo"]];
//
//                   [agencyImgView2 sd_setImageWithURL:[[[result valueForKey:@"agencies"] valueForKey:@"agencylogo"] lastObject] placeholderImage:[UIImage imageNamed:@"dummy_logo"]];
                     
                                          //phoneNumberLb.text =
                     
                     


                 });
                 
                 projectNameLb.text = [result valueForKey:@"projectname"];
                 
                 projectLocationLb.text = [result valueForKey:@"address"];
                 
                 projectTimingLb.text = [NSString stringWithFormat:@"%@ - %@",[result valueForKey:@"starttime"],[result valueForKey:@"endtime"]];
                 

                 
                 
             }
             else
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert !" message:[result valueForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 
                 [alert show];
             }
             
             
             
             
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
}

#pragma mark -- custom Layer
-(void)customeLayer:(UIButton*)button{
    
    button.layer.cornerRadius = 30;
    
    button.layer.masksToBounds = YES;
}

#pragma mark -- 
-(void)buttonSubmit:(id)sender{
    
    UIButton *btn = (UIButton*)sender;
    
    AllAgentVC *vc = [[AllAgentVC alloc]
                             initWithNibName:@"AllAgentVC" bundle:nil];
    
    vc.projectID = projectID;
    
    vc.agencyID = [[[responseDict valueForKey:@"agencies"] objectAtIndex:btn.tag] valueForKey:@"agencyid"];
    
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)setdownloadedImage:(UIButton*)button downloadFromURl:(NSString*)strUrl
{
    [button setImage:[UIImage imageNamed:@"building.jpeg"] forState:UIControlStateNormal];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURL  *url  = [NSURL URLWithString:strUrl];
        
        NSData *data = [[NSData alloc]initWithContentsOfURL:url];
        
        UIImage *img = [[UIImage alloc]initWithData:data];
        
        [button setImage:img forState:UIControlStateNormal];
        
        //          [button.imageView sd_setImageWithURL:[[[responseDict valueForKey:@"agencies"] valueForKey:@"agencylogo"] objectAtIndex:0] placeholderImage:[UIImage imageNamed:@"building.jpeg"]];
        
    });

}
@end
