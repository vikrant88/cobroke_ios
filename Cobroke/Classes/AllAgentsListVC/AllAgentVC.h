//
//  AllAgentVC.h
//  Cobroke
//
//  Created by Vikrant on 16/05/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllAgentVC : UIViewController
{
    
    NSDictionary *responseDict;
    
    IBOutlet UIView *contentView;
    
    IBOutlet UIScrollView *scrollView;
}

@property(assign,nonatomic)NSInteger projectID;

@property(assign,nonatomic)NSString *agencyID;

@end
