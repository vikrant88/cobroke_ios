//
//  AllAgentVC.m
//  Cobroke
//
//  Created by Vikrant on 16/05/17.
//  Copyright © 2017 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#define IS_IPHONE_6 ( [[UIScreen mainScreen] bounds].size.height == 667.0f)

#import "AllAgentVC.h"
#import "ServiceHelper.h"
#import "PhoneNumberViewController.h"

@interface AllAgentVC (){
    
    CGRect leaderLastFrame;
    
    CGRect micLastFrame;
    
    CGRect taggerLastFrame;
    
    CGRect coreTeamLastFrame;
    
    CGRect agentLastFrame;
    
    CGRect lastFrame;
    
}

@end

@implementation AllAgentVC
@synthesize projectID,agencyID;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    [self getAllAgents];
    
   
    
}



-(void)viewDidAppear:(BOOL)animated
{
    
    
    //CGRect frame = CGRectMake(self.view.frame.size.width / 2 - 100, 100, 120, 150);
    
    long count = [[[responseDict valueForKey:@"allagents"] valueForKey:@"mic"] count];
    if (count > 0)
    {
        
        [self setTitle:@"MIC" setFrame:CGRectMake(contentView.frame.size.width/2 - 100, 0, 200, 20)];

    }
    
    for (int i = 0; i < count; i++)
    {
        
        NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"mic"] firstObject] valueForKey:@"firstname"];
        
        NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"mic"] firstObject] valueForKey:@"phoneno"];
        
        NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"mic"] firstObject] valueForKey:@"image"];
        
        CGRect frame =  CGRectMake(contentView.frame.size.width/2 - (IS_IPHONE_6?45:50) , 30, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
        [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
        
         lastFrame = frame;

    }
    
    
    long leaderCount = [[[responseDict valueForKey:@"allagents"] valueForKey:@"leader"] count];
    
    if (leaderCount > 0)
    {
        
        [self setTitle:@"Leader" setFrame:CGRectMake(contentView.frame.size.width/2 - 100, lastFrame.origin.y+lastFrame.size.height+10, 200, 20)];
        
    }
    
    for (int i = 0; i < leaderCount; i++)
    {
        
        
        if (i == 0)
        {
            CGRect frame;
            if (leaderCount <= 3)
            {
                
                if (leaderCount == 1 )
                {
                      frame =  CGRectMake(contentView.frame.size.width/2 - (IS_IPHONE_6?45:50) , lastFrame.origin.y+lastFrame.size.height+10+30, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
                }
                else if(leaderCount == 2)
                {
                      frame =  CGRectMake(contentView.frame.size.width/2 - (IS_IPHONE_6?90:100) , lastFrame.origin.y+lastFrame.size.height+10+30, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
                }
                else if(leaderCount == 3)
                {
                      frame =  CGRectMake(contentView.frame.size.width/2 - (IS_IPHONE_6?135:150) , lastFrame.origin.y+lastFrame.size.height+10+30, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
                }
            }
            else
            {
                   frame =  CGRectMake(IS_IPHONE_6?5:5 , lastFrame.size.height + lastFrame.origin.y+10+30, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
            }
           
            
//            CGRect frame =  CGRectMake(contentView.frame.size.width/2 - 50 , lastFrame.origin.y+lastFrame.size.height, 100, 120);
            
            leaderLastFrame = frame;
            
            
            NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"leader"] firstObject] valueForKey:@"firstname"];
            
            NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"leader"] firstObject] valueForKey:@"phoneno"];
            
             NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"leader"] firstObject] valueForKey:@"image"];
            
            
            [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
            
            
            lastFrame = frame;
            
        }
        else
        {
            CGFloat differ = (contentView.frame.size.width - (leaderLastFrame.origin.x + leaderLastFrame.size.width));
            
            CGFloat size = IS_IPHONE_6?90:100;
            
            if (differ  > size)
            {
                CGRect frame =  CGRectMake(leaderLastFrame.size.width + leaderLastFrame.origin.x , leaderLastFrame.origin.y, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
                
                NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"leader"] objectAtIndex:i] valueForKey:@"firstname"];
                
                NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"leader"] objectAtIndex:i] valueForKey:@"phoneno"];
                
                NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"leader"] objectAtIndex:i] valueForKey:@"image"];
                
                
                [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
                
                leaderLastFrame = frame;
                
                lastFrame = frame;
            }
            else
            {
                CGRect frame =  CGRectMake(IS_IPHONE_6?5:5 , leaderLastFrame.size.height + leaderLastFrame.origin.y, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
                
                NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"leader"] objectAtIndex:i] valueForKey:@"firstname"];
                
                NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"leader"] objectAtIndex:i] valueForKey:@"phoneno"];
                
                 NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"leader"] objectAtIndex:i] valueForKey:@"image"];
                
                
                [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
                
                leaderLastFrame = frame;
                
                lastFrame = frame;
            }
            
            
            
            
            
        }

        
        
        
        
        
        
        
    }
    
    long taggerCount = [[[responseDict valueForKey:@"allagents"] valueForKey:@"tagger"] count];
    
    if (taggerCount > 0)
    {
         [self setTitle:@"Tagger" setFrame:CGRectMake(contentView.frame.size.width/2 - 100, lastFrame.origin.y+lastFrame.size.height+ 10, 200, 20)];
    }
    
    for (int i = 0; i < taggerCount; i++)
    {
        if (i == 0)
        {
            
            CGRect frame =  CGRectMake(IS_IPHONE_6?5:5 , lastFrame.size.height + lastFrame.origin.y+10+30, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
            
            NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"tagger"] firstObject] valueForKey:@"firstname"];
            
            NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"tagger"] firstObject] valueForKey:@"phoneno"];
            
             NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"tagger"] objectAtIndex:i] valueForKey:@"image"];
            
            
            [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
            
            taggerLastFrame = frame;
            
            lastFrame = frame;

        }
        else
        {
            CGFloat differ = (contentView.frame.size.width - (taggerLastFrame.origin.x + taggerLastFrame.size.width));
            
            CGFloat size = IS_IPHONE_6?90:100;
            
            if (differ  > size)
            {
                CGRect frame =  CGRectMake(taggerLastFrame.size.width + taggerLastFrame.origin.x , taggerLastFrame.origin.y, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
                
                NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"tagger"] objectAtIndex:i] valueForKey:@"firstname"];
                
                NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"tagger"] objectAtIndex:i] valueForKey:@"phoneno"];
                
                 NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"tagger"] objectAtIndex:i] valueForKey:@"image"];
                
                
                [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
                
                taggerLastFrame = frame;
                
                 lastFrame = frame;
            }
            else
            {
                CGRect frame =  CGRectMake(IS_IPHONE_6?5:5 , taggerLastFrame.size.height + taggerLastFrame.origin.y, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
                
                NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"tagger"] objectAtIndex:i] valueForKey:@"firstname"];
                
                NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"tagger"] objectAtIndex:i] valueForKey:@"phoneno"];
                
                 NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"tagger"] objectAtIndex:i] valueForKey:@"image"];
                
                
                [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
                
                taggerLastFrame = frame;
                
                 lastFrame = frame;
            }
            
           

          

        }
        
    }
    
    
    long coreteamCount = [[[responseDict valueForKey:@"allagents"] valueForKey:@"coreteam"] count];
    
    if (coreteamCount > 0)
    {
        [self setTitle:@"Core Team" setFrame:CGRectMake(contentView.frame.size.width/2 - 100, lastFrame.origin.y+lastFrame.size.height+ 10, 200, 20)];
    }
    
    for (int i = 0; i < coreteamCount; i++)
    {
        if (i == 0)
        {
            CGRect frame =  CGRectMake(IS_IPHONE_6?10:10, lastFrame.size.height + lastFrame.origin.y + 30, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
            
            NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"coreteam"] firstObject] valueForKey:@"firstname"];
            
            NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"coreteam"] firstObject] valueForKey:@"phoneno"];
            
             NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"coreteam"] objectAtIndex:i] valueForKey:@"image"];
            
            
            [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
            
            coreTeamLastFrame = frame;
            
             lastFrame = frame;
        }
        else
        {
            CGFloat differ = (contentView.frame.size.width - (coreTeamLastFrame.origin.x + coreTeamLastFrame.size.width));
            
            CGFloat size = IS_IPHONE_6?90:100;

            if (differ  > size)
            {
                CGRect frame =  CGRectMake(coreTeamLastFrame.size.width + coreTeamLastFrame.origin.x , coreTeamLastFrame.origin.y, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
                
                NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"coreteam"] objectAtIndex:i] valueForKey:@"firstname"];
                
                NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"coreteam"] objectAtIndex:i] valueForKey:@"phoneno"];
                
                 NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"coreteam"] objectAtIndex:i] valueForKey:@"image"];
                
                [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
                
                coreTeamLastFrame = frame;

                
                 lastFrame = frame;
            }
            else
            {
                CGRect frame =  CGRectMake(IS_IPHONE_6?10:10 , coreTeamLastFrame.size.height + coreTeamLastFrame.origin.y, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
                
                NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"coreteam"] objectAtIndex:i] valueForKey:@"firstname"];
                
                NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"coreteam"] objectAtIndex:i] valueForKey:@"phoneno"];
                
                 NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"coreteam"] objectAtIndex:i] valueForKey:@"image"];
                
                
                [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
                
                coreTeamLastFrame = frame;
                
                 lastFrame = frame;
            }
            
            
            
        }
        
    }
    
    long agentCount = [[[responseDict valueForKey:@"allagents"] valueForKey:@"agent"] count];
    
    
    if (agentCount > 0)
    {
        [self setTitle:@"Agents" setFrame:CGRectMake(contentView.frame.size.width/2 - 100, lastFrame.origin.y+lastFrame.size.height+ 10, 200, 20)];
    }
    for (int i = 0; i < agentCount; i++)
    {
        if (i == 0)
        {
            CGRect frame =  CGRectMake(5 , lastFrame.size.height + lastFrame.origin.y + 30, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
            
            NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"agent"] objectAtIndex:i] valueForKey:@"firstname"];
            
            NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"agent"] objectAtIndex:i] valueForKey:@"phoneno"];
            
             NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"agent"] objectAtIndex:i] valueForKey:@"image"];
            
            
            [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
            
            agentLastFrame = frame;
            
             lastFrame = frame;
        }
        else
        {
            
            CGFloat differ = (contentView.frame.size.width - (agentLastFrame.origin.x + agentLastFrame.size.width));
            CGFloat size = IS_IPHONE_6?90:100;
            
            if (differ  > size)
            {
                
                CGRect frame =  CGRectMake(agentLastFrame.size.width + agentLastFrame.origin.x , agentLastFrame.origin.y , IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
                
                NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"agent"] objectAtIndex:i] valueForKey:@"firstname"];
                
                NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"agent"] objectAtIndex:i] valueForKey:@"phoneno"];
                
                 NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"agent"] objectAtIndex:i] valueForKey:@"image"];
                
                
                [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
                
                agentLastFrame = frame;
                
                 lastFrame = frame;
            }
            else
            {
                CGRect frame =  CGRectMake(5 , agentLastFrame.size.height + agentLastFrame.origin.y, IS_IPHONE_6?90:100, IS_IPHONE_6?110:120);
                
                NSString *empName = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"agent"] objectAtIndex:i] valueForKey:@"firstname"];
                
                NSString *contact = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"agent"] objectAtIndex:i] valueForKey:@"phoneno"];
                
                 NSString *ImageUrl = [[[[responseDict valueForKey:@"allagents"] valueForKey:@"agent"] objectAtIndex:i] valueForKey:@"image"];
                
                [self addMICView:frame title:empName contact:contact imageUrl:ImageUrl];
                
                agentLastFrame = frame;
                
                 lastFrame = frame;
            }
            
            
            
        }
        
    }
   [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, lastFrame.origin.y+lastFrame.size.height+20)];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action

- (IBAction)backTap:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- Get Single Project
-(void)getAllAgents
{
    [[ServiceHelper sharedManager] setHudShow:YES];
    
    
    NSString *oauthToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"authToken"];
    
       NSDictionary *param = @{@"oauth":oauthToken,@"projectid": [NSString stringWithFormat:@"%lu",(unsigned long)projectID],@"agencyid": agencyID };
    
    [[ServiceHelper sharedManager] sendRequestWithMethodName:@"marketingagencyagents" setHTTPMethod:@"POST" params:param completion:^(NSDictionary *result)
     {
         [[ServiceHelper sharedManager] setHudShow:NO];
         
         responseDict = result;
         NSLog(@"response ...  %@",result);
         
         if (result)
         {
             
             
             NSString *status = [result valueForKey:@"status"];
             if ([status isEqualToString:@"true"])
             {
                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                 });
                 
              
                 
                 
                 
                 
             }
             else
             {
                 
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Title" message:[result valueForKey:@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                 
                 [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                     
                     NSLog(@"go to home page");
                     if ([[result valueForKey:@"msg"] isEqualToString:@"Invalid Token."])
                     {
                         
                         
                         [[NSUserDefaults standardUserDefaults] setObject:@"logout" forKey:@"isLogin"];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"authToken"];
                         
                         
                         PhoneNumberViewController *pVc = [[PhoneNumberViewController alloc] initWithNibName:@"PhoneNumberViewController" bundle:nil];
                         
                         [self.navigationController pushViewController:pVc animated:YES];
                         
                         
                     }
                     
                 }]];
                 
                 [self presentViewController:alertController animated:YES completion:nil];
                 
             }
             
             
             
             
             
         }
         else
         {
             [[ServiceHelper sharedManager] setHudShow:NO];
             NSLog(@"Login unsuccess.... ");
             
         }
         
     }];
}
-(void)addMICView:(CGRect)dynamicFrame title:(NSString*)empName contact:(NSString*)phoneNo imageUrl:(NSString*)url {
    
    UIView *view = [[UIView alloc]init];
    
    view.frame = dynamicFrame;
    
    
    UILabel *title = [[UILabel alloc] init];
    
    title.frame = CGRectMake(0, 0, view.frame.size.width, 15);
    
    title.backgroundColor = [UIColor clearColor];
    
    title.textColor = [UIColor blackColor];
    
    title.textAlignment = NSTextAlignmentCenter;
    
    [title setFont:[UIFont systemFontOfSize:10]];
    
    title.text = empName;
    
    //[view addSubview:title];
    
    
    UIImageView *imgView = [[UIImageView alloc] init];
    
    imgView.frame = CGRectMake(view.frame.size.width / 2 - (IS_IPHONE_6?30.0:40.0), 0, IS_IPHONE_6?60:80, IS_IPHONE_6?60:80);
    
    imgView.layer.cornerRadius = IS_IPHONE_6?30:40;
    
    imgView.layer.masksToBounds = true;
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        //            NSURL  *url  = [NSURL URLWithString:[[[responseDict valueForKey:@"agencies"] valueForKey:@"agencylogo"] firstObject]];
        //
        //            NSData *data = [[NSData alloc]initWithContentsOfURL:url];
        //
        //            UIImage *img = [[UIImage alloc]initWithData:data];
        //
        //            [button setImage:img forState:UIControlStateNormal];
        
        
        [imgView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"building.jpeg"]];
        
        
        
        
    });

    
    [view addSubview:imgView];
    
    
    
    UILabel *nameLb = [[UILabel alloc] init];
    
    nameLb.text = empName;
    
    [nameLb setFont:[UIFont systemFontOfSize:10]];

    
    nameLb.textAlignment = NSTextAlignmentCenter;

    nameLb.backgroundColor = [UIColor clearColor];

    
    nameLb.frame = CGRectMake(0, CGRectGetMaxY(imgView.frame)+5, view.frame.size.width, 15);
    
    nameLb.textColor = [UIColor darkGrayColor];
    
    [view addSubview:nameLb];
    
    
    UILabel *phnLb = [[UILabel alloc] init];
    
    phnLb.frame = CGRectMake(0, CGRectGetMaxY(nameLb.frame), view.frame.size.width, 15);
    
    phnLb.backgroundColor = [UIColor clearColor];
    
    [phnLb setFont:[UIFont systemFontOfSize:10]];

    phnLb.textColor = [UIColor darkGrayColor];
    
    phnLb.text = phoneNo;
    
    phnLb.textAlignment = NSTextAlignmentCenter;

    [view addSubview:phnLb];
    
    view.backgroundColor = [UIColor clearColor];
    
    [contentView addSubview: view];
    
    
    
    
    
}
-(void)setTitle:(NSString *)textTitle setFrame:(CGRect)frame {
    
        UILabel *title = [[UILabel alloc] init];
        
        title.frame = frame;
        
        title.backgroundColor = [UIColor clearColor];
        
        title.textColor = [UIColor blackColor];
        
        title.textAlignment = NSTextAlignmentCenter;
        
        [title setFont:[UIFont systemFontOfSize:15]];
        
        title.text = textTitle;
        
        [contentView addSubview: title];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
